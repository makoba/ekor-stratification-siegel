%%%Brackets
\DeclarePairedDelimiter{\roundbr}{(}{)}
\DeclarePairedDelimiter{\squarebr}{[}{]}
\DeclarePairedDelimiter{\doublesquarebr}{\llbracket}{\rrbracket}
\DeclarePairedDelimiter{\curlybr}{\{}{\}}
\DeclarePairedDelimiter{\anglebr}{\langle}{\rangle}

\DeclarePairedDelimiter{\abs}{\lvert}{\rvert}

\DeclarePairedDelimiterX{\set}[2]{\{}{\}}{#1 \;\delimsize\vert\; #2}

\newcommand*{\blank}{\text{-}}

\newcommand*{\bbM}{\mathbb{M}}
\newcommand*{\rmM}{\mathrm{M}}

\newcommand*{\bbL}{\mathbb{L}}

\newcommand*{\id}{\mathrm{id}}
\newcommand*{\op}{\mathrm{op}}

\DeclareMathOperator{\eq}{eq}
\DeclareMathOperator{\im}{im}
\DeclareMathOperator{\coker}{coker}
\DeclareMathOperator{\gr}{gr}
\DeclareMathOperator{\fib}{fib}

\DeclareMathOperator{\Frac}{Frac}
\DeclareMathOperator{\Gal}{Gal}

\DeclareMathOperator{\Fun}{Fun}
\DeclareMathOperator{\Hom}{Hom}
\DeclareMathOperator{\End}{End}
\DeclareMathOperator{\Aut}{Aut}
\DeclareMathOperator{\Isom}{Isom}

\DeclareMathOperator{\iHom}{\mathcal{H}\!\mathit{om}}
\DeclareMathOperator{\iIsom}{\mathcal{I}\!\mathit{som}}

\newcommand*{\bfK}{\mathbf{K}}

\newcommand*{\bsfC}{\catbname{C}}
\DeclareMathOperator{\CMon}{CMon}

\DeclareMathOperator{\Cont}{Cont}


\newcommand*{\calA}{\mathcal{A}}
\newcommand*{\calB}{\mathcal{B}}
\newcommand*{\calC}{\mathcal{C}}
\newcommand*{\calD}{\mathcal{D}}
\newcommand*{\calE}{\mathcal{E}}
\newcommand*{\calF}{\mathcal{F}}
\newcommand*{\calG}{\mathcal{G}}
\newcommand*{\calH}{\mathcal{H}}
\newcommand*{\calI}{\mathcal{I}}
\newcommand*{\calJ}{\mathcal{J}}
\newcommand*{\calK}{\mathcal{K}}
\newcommand*{\calL}{\mathcal{L}}
\newcommand*{\calM}{\mathcal{M}}
\newcommand*{\calN}{\mathcal{N}}
\newcommand*{\calO}{\mathcal{O}}
\newcommand*{\calP}{\mathcal{P}}
\newcommand*{\calQ}{\mathcal{Q}}
\newcommand*{\calR}{\mathcal{R}}
\newcommand*{\calS}{\mathcal{S}}
\newcommand*{\calT}{\mathcal{T}}
\newcommand*{\calU}{\mathcal{U}}
\newcommand*{\calV}{\mathcal{V}}
\newcommand*{\calW}{\mathcal{W}}
\newcommand*{\calX}{\mathcal{X}}
\newcommand*{\calY}{\mathcal{Y}}
\newcommand*{\calZ}{\mathcal{Z}}

\newcommand*{\fraka}{\mathfrak{a}}
\newcommand*{\frakb}{\mathfrak{b}}

\newcommand*{\ZZ}{\mathbf{Z}}
\newcommand*{\QQ}{\mathbf{Q}}
\newcommand*{\NN}{\mathbf{N}}

\newcommand*{\finadeles}{\mathbf{A}_f}
\newcommand*{\Zhat}{\widehat{\ZZ}}

\newcommand*{\Fp}{\mathbf{F}_p}
\newcommand*{\Zp}{\mathbf{Z}_p}
\newcommand*{\Qp}{\mathbf{Q}_p}

\newcommand*{\Qpbar}{\overline{\mathbf{Q}}_p}
\newcommand*{\Fpbar}{\overline{\mathbf{F}}_p}
\newcommand*{\Qpbrev}{\breve{\mathbf{Q}}_p}
\newcommand*{\Zpbrev}{\breve{\mathbf{Z}}_p}


\newcommand*{\catname}[1]{\mathsf{#1}}
\newcommand*{\catbname}[1]{\bm{\mathsf{#1}}}

\newcommand*{\catgrps}{\catbname{Grp}}
\newcommand*{\catalgs}{\catname{Alg}}
\newcommand*{\catnilp}{\catname{Nilp}}
\newcommand*{\catgrpds}{\catbname{Grpd}}
\newcommand*{\catcats}{\catbname{Cat}}
\newcommand*{\catsymmoncats}{\catbname{SymMonCat}}

\newcommand*{\catpol}{\catname{Pol}}
\newcommand*{\cathpol}{\catname{HPol}}

\newcommand*{\catstacks}{\catbname{Stack}}

\newcommand*{\catabvars}{\catname{AbVar}}
\newcommand*{\catchabvars}{\catname{ChAbVar}}
\newcommand*{\catchpdiv}{\catname{ChBT}}
\newcommand*{\catpolchpdiv}{\catname{PolChBT}}
\newcommand*{\catpdiv}{\catname{BT}}

\newcommand*{\catvects}{\catname{Vect}}
\newcommand*{\catgrass}{\catname{Grass}}
\newcommand*{\catWvects}{W \blank \catname{Vect}}
\newcommand*{\catWnvects}[1]{W_{#1} \blank \catname{Vect}}

\newcommand*{\catmods}{\catname{Mod}}
\newcommand*{\catWmods}{W \blank \catname{Mod}}

\newcommand*{\catlattch}{\catname{LattCh}}

\newcommand*{\catpairs}{\catname{Pair}}
\newcommand*{\catdisps}{\catname{Disp}}
\newcommand*{\DD}{\mathbf{D}}

\newcommand*{\catchvects}{\catname{ChVect}}
\newcommand*{\catchpairs}{\catname{ChPair}}
\newcommand*{\catchdisps}{\catname{ChDisp}}

\newcommand*{\catpolchvects}{\catname{PolChVect}}
\newcommand*{\catpolchpairs}{\catname{PolChPair}}
\newcommand*{\catpolchdisps}{\catname{PolChDisp}}

\newcommand*{\cathpolchvects}{\catname{HPolChVect}}
\newcommand*{\cathpolchpairs}{\catname{HPolChPair}}
\newcommand*{\cathpolchdisps}{\catname{HPolChDisp}}

\newcommand*{\catzips}{\catname{Zip}}
\newcommand*{\catpolzips}{\catname{PolZip}}
\newcommand*{\cathpolzips}{\catname{HPolZip}}
\newcommand*{\catchzips}{\catname{ChZip}}
\newcommand*{\catpolchzips}{\catname{PolChZip}}
\newcommand*{\cathpolchzips}{\catname{HPolChZip}}
\newcommand*{\Sht}{\catname{Sht}}

\newcommand*{\Fl}{\calF \! \ell}

\newcommand*{\dR}{\mathrm{dR}}
\DeclareMathOperator{\Fil}{Fil}
\newcommand*{\sigFil}{\sigma \blank \Fil}
\newcommand*{\divd}{\mathrm{div}}
\newcommand*{\formal}{\mathrm{fml}}
\newcommand*{\nilp}{\mathrm{nilp}}
\newcommand*{\red}{\mathrm{red}}
\newcommand*{\pf}{\mathrm{pf}}
\newcommand*{\std}{\mathrm{std}}
\newcommand*{\pdiv}{\mathrm{BT}}

\DeclareMathOperator{\Spec}{Spec}
\DeclareMathOperator{\Spf}{Spf}
\DeclareMathOperator{\Spd}{Spd}

\newcommand*{\LZ}{\mathrm{LZ}}
\newcommand*{\lift}{\mathrm{lift}}

\newcommand*{\Gm}{\mathbf{G}_m}
\DeclareMathOperator{\GL}{GL}
\DeclareMathOperator{\GSp}{GSp}
\DeclareMathOperator{\SL}{SL}
\DeclareMathOperator{\Sp}{Sp}

\newcommand*{\rdt}{\mathrm{rdt}}
\newcommand*{\loc}{\mathrm{loc}}
\newcommand*{\can}{\mathrm{can}}

\DeclareMathOperator{\Gr}{Gr}
\DeclareMathOperator{\Grass}{Grass}
\DeclareMathOperator{\Adm}{Adm}
\DeclareMathOperator{\Hk}{Hk}

\DeclareMathOperator{\Sh}{Sh}
\DeclareMathOperator{\modA}{A}
\DeclareMathOperator{\EKOR}{EKOR}

