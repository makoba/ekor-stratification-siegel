\section{Introduction}

Fix a rational prime $p$, a positive integer $g$, an auxiliary integer $N \geq 3$ that is not divisible by $p$ and a non-empty subset $J \subseteq \ZZ$ with $J + 2g\ZZ = J$ and $-J = J$.
Then we are interested in studying the \emph{Siegel modular variety with parahoric level structure}
\[
    \calA_{g, J, N}
\]
over $\Zp$.
This is a quasiprojective scheme that parametrizes certain polarized chains of type $J$ of $g$-dimensional Abelian varieties with full level-$N$ structure; its moduli description was first given by de Jong \cite{de-jong} and then in full generality by Rapoport and Zink \cite{rapoport-zink-96}.
The scheme $\calA_{g, J, N}$ is the prime example of an integral model of a \emph{Shimura variety} at a place of parahoric bad reduction.
While the generic fiber of $\calA_{g, J, N}$ is smooth, its special fiber typically is singular.
This is related to the fact that the $p$-torsion of an Abelian variety in characteristic $p$ is not étale.

Now fix an algebraic closure $\Fpbar$ of $\Fp$, let $\Zpbrev \coloneqq W(\Fpbar)$ be the ring of $p$-typical Witt vectors of $\Fpbar$ and write $\Qpbrev \coloneqq \Zpbrev[1/p]$ for its fraction field.
Denote by $\sigma$ the Frobenius on $\Qpbrev$.
Also fix a symplectic $\Qp$-vector space $V$ of dimension $2g$ and a self-dual lattice chain $(\Lambda_i)_{i \in J}$ in $V$.
Then there exists a natural map, called the \emph{central leaves map},
\[
    \Upsilon \colon \calA_{g, J, N} \roundbr[\big]{\Fpbar} \to \breve{K}_{\sigma} \backslash \GSp(V)(\Qpbrev);
\]
here $\breve{K} \subseteq \GSp(V)(\Qpbrev)$ is the stabilizer of the lattice chain $(\Lambda_i)_i$ and modding out by $\breve{K}_{\sigma}$ is notation for taking the quotient by the $\sigma$-conjugation action $g \colon x \mapsto g x \sigma(g)^{-1}$.
It is roughly given by sending a polarized chain of Abelian varieties to the twisted conjugacy class corresponding to the Frobenius $\Phi$ of the associated rational Dieudonné module, see the work of Oort \cite{oort-foliations} and also He and Rapoport \cite{he-rapoport}.
The image of this map is given by $\breve{K}_{\sigma} \backslash X$ where
\[
    X = \bigcup_{w \in \Adm_{g, J}} \breve{K} w \breve{K} \subseteq \GSp(V)(\Qpbrev)
\]
is a finite union of double cosets that is indexed over the so called \emph{admissible set} $\Adm_{g, J}$.

The fibers of the composition
\[
    \lambda \colon \calA_{g, J, N} \roundbr[\big]{\Fpbar} \xrightarrow{\Upsilon} \breve{K}_{\sigma} \backslash X \to \breve{K} \backslash X / \breve{K} \cong \Adm_{g, J}
\]
define a decomposition of $(\calA_{g, J, N})_{\Fpbar}$ into smooth locally closed subschemes, the so-called \emph{Kottwitz-Rapoport (KR) stratification}.
In fact Rapoport and Zink construct the following data.

\begin{itemize}
    \item
    A flat projective scheme $\bbM^{\loc}$ over $\Zp$ with an action of the parahoric $\Zp$-group scheme $\calG$ for $\GSp(V)$ given by the lattice chain $(\Lambda_i)_i$.
    This scheme is called the \emph{local model}; it parametrizes isotropic chains of $g$-dimensional subspaces $C_i \subseteq \Lambda_i$ and satisfies $\bbM^{\loc}(\Fpbar) = \breve{K} \backslash X$.

    \item
    A smooth morphism
    \[
        \calA_{g, J, N} \to \squarebr[\big]{\calG \backslash \bbM^{\loc}}
    \]
    that parametrizes the Hodge filtration in the de Rham cohomology of a polarized chain of Abelian varieties.
    One recovers the map $\lambda$ from above by evaluating this morphism on $\Fpbar$-points, see \cite[Section 7.(ii)]{he-rapoport}.
\end{itemize}

He and Rapoport also consider the composition
\[
    \upsilon \colon \calA_{g, J, N} \roundbr[\big]{\Fpbar} \to \breve{K}_{\sigma} \backslash X \to \breve{K}_{\sigma} \backslash (\breve{K} \backslash X)
\]
where $\breve{K}_1 \subseteq \breve{K}$ is the pro-unipotent radical.
They observe that the codomain of $\upsilon$ is a finite set and call its fibers \emph{Ekedahl-Kottwitz-Oort-Rapoport (EKOR) strata}.

In the hyperspecial case $J = 2g \ZZ$ the EKOR stratification is also just called \emph{Ekedahl-Oort (EO) stratification} and was first considered by Oort \cite{oort-eo}.
Two points in $\calA_{g, 2g\ZZ, N}(\Fpbar)$ given by two (principally polarized) Abelian varieties lie in the same EO stratum if and only if their $p$-torsion group schemes are isomorphic.
Viehmann and Wedhorn \cite{viehmann-wedhorn} realize the EO stratification as the fibers of a smooth morphism from $(\calA_{g, 2g\ZZ, N})_{\Fp}$ into an algebraic stack that parametrizes $F$-zips in the sense of Moonen and Wedhorn \cite{moonen-wedhorn} with symplectic structure.

\begin{question}
    Is it possible for general $J$ to realize the map $\upsilon$, or maybe even $\Upsilon$, as a smooth morphism from $\calA_{g, J, N}$ into some naturally defined algebraic stack?
\end{question}

The existence of such a smooth morphism would in particular give a new proof of the smoothness of the EKOR strata and the closure relations between them.
More importantly, it also provides a tool that may be applied to further study the geometry of $\calA_{g, J, N}$.

\vspace*{1em}

Let us give an overview of results that have been obtained so far (and that we are aware of), also considering more general Shimura varieties than the Siegel modular variety:
\begin{itemize}
    \item
    Moonen and Wedhorn \cite{moonen-wedhorn} introduce the notion of an $F$-zip that is a characteristic $p$ analogue of the notion of a Hodge structure.
    Given an Abelian variety $A$ over some $\Fp$-algebra $R$ the de Rham cohomology $H^1_{\dR}(A/R)$ naturally carries the structure of an $F$-zip.
    If $R$ is perfect then the datum of this $F$-zip is equivalent to the datum of the Dieudonné module of the $p$-torsion $A[p]$.
    Pink, Wedhorn and Ziegler \cite{pink-wedhorn-ziegler} define a group theoretic version of the notion of an $F$-zip.

    Viehmann and Wedhorn \cite{viehmann-wedhorn} define a moduli stack of $F$-zips with polarization and endomorphism structure in a PEL-type situation with hyperspecial level structure.
    They construct a morphism from the special fiber of the associated Shimura variety to this stack, parametrizing the EO stratification.
    They then show that this morphism is flat and use this to show that the EO strata are non-empty and quasi-affine and to compute their dimension.
    The smoothness of the EO strata was already shown by Vasiu \cite{vasiu-2}.

    Zhang \cite{zhang-eo} constructs a morphism from the special fiber of the Kisin integral model, see \cite{kisin-integral-models}, of a Hodge type Shimura variety to a stack of group-theoretic zips.
    They then show that this morphism is smooth and thus gives an EO stratification with the desired properties.
    Shen and Zhang \cite{shen-zhang-eo} later generalize this to Shimura varieties of Abelian type.

    Shen, Yu and Zhang \cite{shen-yu-zhang} further generalize this to Shimura varieties of Abelian type at parahoric level, where the construction of integral models is due to Kisin and Pappas, see \cite{kisin-pappas-integral-models}.
    However, they only construct a morphism from each KR stratum of the special fiber of the Shimura variety into a certain stack of group-theoretic zips, parametrizing the EKOR strata contained in this KR stratum.
    Still, they show that this morphism is smooth, thus establishing the smoothness of the EKOR strata.

    Hesse \cite{hesse} considers an explicit moduli stack of polarized chains of $F$-zips and constructs a morphism from the Siegel modular variety into this stack.
    However it appears that such a morphism is not well-behaved in the general parahoric situation, see \Cref{rmk:chains-lau-zink}.

    \item
    Xiao and Zhu \cite{xiao-zhu} consider a perfect moduli stack of mixed characteristic local shtukas as well as restricted versions in a hyperspecial situation.
    For a Shimura variety of Hodge type they construct a morphism from the perfection of its special fiber into the stack of local shtukas and show that it realizes the central leaves map.
    They state that the induced morphism to the moduli stack of restricted local shtukas is perfectly smooth, see \cite[Proposition 7.2.4]{xiao-zhu}, but the proof appears to be incomplete because the diagram used there is not commutative as required.

    Shen, Yu and Zhang \cite{shen-yu-zhang} again generalize this to the parahoric situation.
    Similarly as Xiao and Zhu they construct a morphism from the perfected special fiber of the Shimura variety to their stack of local shtukas and state that the induced morphism to the moduli stack of restricted local shtukas is perfectly smooth, but their proof \cite[Theorem 4.4.3]{shen-yu-zhang} employs a similar non-commutative diagram.
    This issue is addressed in the errata \cite{shen-yu-zhang-errata}, where Shen, Yu and Zhang state that their proof works with slight modifications; it is not clear to the author why the argument offered in the errata is sufficient to complete the proof.


    \item
    Zink \cite{zink-02} introduces the notion of a display that is a non-perfect version of the notion of a Dieudonné module.
    Bültel and Pappas \cite{bueltel-pappas} define a group-theoretic version; this gives a deperfection of the notion of a local shtuka from \cite{xiao-zhu} in the hyperspecial situation.
    In a parahoric Hodge type situation Pappas \cite{pappas-parahoric-disps} also gives a definition of group-theoretic displays, however only over $p$-torsionfree $p$-adic rings.
    Pappas also constructs a group-theoretic display on the $p$-completion of a Kisin-Pappas integral model.

    % \item
    % Scholze and Weinstein (\cite{scholze-weinstein}) define a v-stack of $\calG$-shtukas (of type $\mu$) over $\Spd(\Zp)$, the special fiber of which agrees with the stack $\Sht^{\loc}_{K, \mu}$ of \cite{shen-yu-zhang}.
    % It is not completely clear what the relation between shtukas and displays is, but see the article \cite{bartling} of Bartling.

    % Pappas and Rapoport (\cite{pappas-rapoport-shtukas}) construct, in a Hodge type situation with parahoric level structure, a morphism from the (v-sheaf associated to the) integral model of the Shimura variety to this stack of $\calG$-shtukas.
\end{itemize}

\subsection{Overview}

Let us explain the content of the present work.
The letter $R$ denotes a $p$-nilpotent ring.

We start by recalling the definition of a (3n-)display in the sense of \cite{zink-02}.

\begin{definition}[\ref{def:disps}] \label{intro-def:disps}
    Let $0 \leq d \leq h$ be integers.
    Then a \emph{display of type $(h, d)$ over $R$} is a tuple $(M, M_1, \Psi)$ that is given as follows.
    \begin{itemize}
        \item
        $M$ is a finite projective $W(R)$-module of rank $h$.

        \item
        $M_1 \subseteq M$ is a $W(R)$-submodule containing $I_R M$ and such that $M_1/I_R M \subseteq M/I_R M$ is a direct summand of rank $d$.

        \item
        $\Psi \colon \widetilde{M_1} \to M$ is an isomorphism of $W(R)$-modules that we call the \emph{divided Frobenius}.
    \end{itemize}
    Here $W(R)$ denotes the ring of Witt vectors of $R$, $I_R$ denotes the kernel of the projection $W(R) \to R$ and the object $\widetilde{M_1}$ is a certain finite projective $W(R)$-module attached to $(M, M_1)$, see \Cref{def:m-1-tilde} for more details.
    When $R$ is a perfect ring of characteristic $p$ then $\widetilde{M_1}$ agrees with the Frobenius-twist $M_1^{\sigma}$.

    The category of displays carries a natural duality and an action of the symmetric monoidal category of tuples $(I, \iota)$ consisting of an invertible $W(R)$-module $I$ and an isomorphism $\iota \colon I^{\sigma} \to I$.
\end{definition}

For positive integers $m$ and $n$ with $m \geq n + 1$ we define the notion of an $(m, n)$-truncated displays that is inspired by the restricted local shtukas from \cite[Definition 5.3.1]{xiao-zhu}.
The word \enquote{truncated} refers to the use of truncated Witt vectors, the numbers $m$ and $n$ roughly measures how truncated the module $M$ and the divided Frobenius $\Psi$ is.
One should note that this notion of an $(m, n)$-truncated display is different from the notion of an $n$-truncated display as defined by Lau and Zink in \cite{lau-zink-18}, but see \Cref{rmk:relation-lau-zink} for a comparison.
For us it will be crucial to work with the $(m, n)$-truncated objects, see \Cref{rmk:chains-lau-zink}.

The following theorem gives a relation between displays and $p$-divisible groups.

\begin{theorem}[{\cite[Theorem 5.1]{lau-10}}, \ref{thm:disp-p-div}] \label{intro-thm:disp-p-div}
    There is a natural functor
    \[
        \DD \colon \curlybr[\big]{\text{$p$-divisible groups of height $h$ and dimension $d$ over $R$}}^{\op} \to \curlybr[\big]{\text{displays of type $(h, d)$ over $R$}}.
    \]
   that restricts to an equivalence between formal $p$-divisible groups and $F$-nilpotent displays.
\end{theorem}

As we are interested in studying the moduli space $\calA_{g, J, N}$ of polarized chains of Abelian varieties it makes sense to make the following definition.

\begin{definition}[\ref{def:polarized-chains}] \label{intro-def:chains-disps}
    A \emph{homogeneously polarized chain of displays of type $(g, J)$ over $R$} is a tuple
    \[
        \roundbr[\big]{(M_i)_i, (\rho_{i, j})_{i, j}, (\theta_i)_i, (M_{i, 1})_i, (\Psi_i)_i, I, \iota, (\lambda_i)_i}
    \]
    that is given as follows.
    \begin{itemize}
        \item
        $((M_i, M_{i, 1}, \Psi_i)_i, (\rho_{i, j})_{i, j})$ is a diagram of shape $J$ in the category of displays of type $(2g, J)$ such that the homomorphism of $R$-modules
        \[
            \rho_{i, j} \colon R \otimes_{W(R)} M_i \to R \otimes_{W(R)} M_j
        \]
        is of constant rank $2g - (j - i)$ for all $i \leq j \leq i + 2g$.

        \item
        $\theta_i \colon (M_i, M_{i, 1}, \Psi_i) \to (M_{i + 2g}, M_{i + 2g, 1}, \Psi_{i + 2g})$ is an isomorphism such that we have the compatibilities $\theta_j \circ \rho_{i, j} = \rho_{i + 2g, j + 2g} \circ \theta_i$ and $\rho_{i, i + 2g} = p \theta_i$.

        \item
        $(I, \iota)$ is as in \Cref{intro-def:disps}.

        \item
        $\lambda_i \colon (M_i, M_{i, 1}, \Psi_i) \to (I, \iota) \otimes (M_{-i}, M_{-i, 1}, \Psi_{-i})^{\vee}$ is an antisymmetric isomorphism such that we have $\lambda_j \circ \rho_{i, j} = (\id_{(I, \iota)} \otimes \rho_{-j, -i}^{\vee}) \circ \lambda_i$.
    \end{itemize}
\end{definition}

We again also give an $(m, n)$-truncated version of this definition.
When $R$ is of characteristic $p$ then we allow $n$ to take the additional value $1 \blank \rdt$ that can be thought of as being slightly smaller than $1$; \enquote{$\rdt$} refers to the term \enquote{reductive quotient}.
Roughly the case $n = 1 \blank \rdt$ corresponds to only having a divided Frobenius on the graded pieces of the $1$-truncated chain of modules, see \Cref{def:chains}.

We then show that the stack $\cathpolchdisps_{g, J}^{(m, n)}$ of $(m, n)$-truncated homogeneously polarized chains of displays over $\Spf(\Zp)$ admits a quotient stack description.

\begin{proposition}[\ref{lem:polarized-disps-quotient-stack}] \label{intro-lem:hpolchainsdisps}
    There exists an equivalence
    \[
        \cathpolchdisps^{(m, n)}_{g, J} \to \squarebr[\Big]{\roundbr[\big]{L^{(m)} \calG}_{\Delta} \backslash \rmM^{\loc, (n)}},
    \]
    where we use the following notation.
    \begin{itemize}
        \item
        $L^{(m)} \calG$ denotes the $m$-truncated Witt vector positive loop group of $\calG$, see \Cref{subsec:witt-vectors}.

        \item
        $\rmM^{\loc}$ is the $p$-completion of the local model $\bbM^{\loc}$ and $\rmM^{\loc, (n)} \to \rmM^{\loc}$ is a certain $L^{(m)} \calG$-equivariant $L^{(n)} \calG$-torsor, see \Cref{def:m-loc-sp-+}.

        \item
        The subscript $\Delta$ indicates that we take the quotient by the diagonal action.
    \end{itemize}
\end{proposition}
Thus our definition of $\cathpolchdisps^{(m, n)}_{g, J}$ gives (up to a slight difference in normalization) a deperfection of the stack of parahoric restricted local shtukas from \cite[Section 4]{shen-yu-zhang},
see \Cref{rmk:relation-shtukas-2}.
In particular we obtain bijections
\[
    \cathpolchdisps_{g, J} \roundbr[\big]{\Fpbar} \to \breve{K}_{\sigma} \backslash X
    \quad \text{and} \quad
    \cathpolchdisps_{g, J}^{(m, 1 \blank \rdt)} \roundbr[\big]{\Fpbar} \to \breve{K}_{\sigma} \backslash (\breve{K}_1 \backslash X).
\]

Applying \Cref{intro-thm:disp-p-div} to the moduli description of $\calA_{g, J, N}$ we thus obtain a natural morphism
\[
    \Upsilon \colon \calA_{g, J, N}^{\wedge} \to \cathpolchdisps_{g, J}
\]
that realizes the central leaves map, see \Cref{def:upsilon}; here $\calA_{g, J, N}^{\wedge}$ denotes the $p$-completion of $\calA_{g, J, N}$.
For any $m \geq 2$ the composition
\[
    \roundbr[\big]{\calA_{g, J, N}}_{\Fp} \xrightarrow{\Upsilon} \cathpolchdisps_{g, J, \Fp} \to \cathpolchdisps_{g, J}^{(m, 1 \blank \rdt)}
\]
then realizes the map $\upsilon$ parametrizing the EKOR stratification.
Our main result is now the following.

\begin{theorem}[\ref{thm:main-res}] \label{intro-thm:main-res}
    For every $(m, n)$ with $n \neq 1 \blank \rdt$ the natural morphism $\calA_{g, J, N}^{\wedge} \to \cathpolchdisps_{g, J}^{(m, n)}$ is smooth.
    Similarly for every $m \geq 2$ also the morphism $(\calA_{g, J, N})_{\Fp} \to \cathpolchdisps_{g, J}^{(m, 1 \blank \rdt)}$ is smooth.
\end{theorem}

\begin{proof}[Strategy of proof]
    By the Serre-Tate theorem the smoothness of the morphism at a point of $\abs{\calA_{g, J, N}^{\wedge}}$ corresponding to a polarized chain of Abelian varieties only depends on the associated polarized chain of $p$-divisible groups.
    Using \Cref{intro-thm:disp-p-div} we then show that the morphism smooth along the formal locus, i.e.\ the locus of chains of Abelian varieties with formal $p$-divisible groups.
    To obtain the smoothness in general we then finally show that there are enough points in $\abs{\calA_{g, J, N}^{\wedge}}$ that specialize into the formal locus, see \Cref{cor:specializing} for a precise statement.
\end{proof}

As a natural next step it would be interesting to generalize our results to the case of a more general Shimura variety at parahoric level instead of the Siegel modular variety.
It could be expected that there exists a stack of $(\calG, \mu)$-displays for every datum $(\calG, \mu)$ consisting of a parahoric $\Zp$-group scheme $\calG$ and a minuscule geometric conjugacy class $\mu$ of cocharacters of the generic fiber of $\calG$, as well as truncated versions that recover the definition from \cite{bueltel-pappas} in the hyperspecial case and give back our definition when the generic fiber of $\calG$ is either a general linear group or a group of symplectic similitudes.
In a situation where the local group datum $(\calG, \mu)$ comes from a Shimura datum there then should be a natural smooth morphism from the $p$-completion of the corresponding Shimura variety into the stack of truncated $(\calG, \mu)$-displays.
As already mentioned above, partial results in this direction have been achieved by Pappas in \cite{pappas-parahoric-disps}.

\subsection{Acknowledgements}

I am very grateful to Ulrich Görtz for introducing me to the subject of Shimura varieties and their special fibers and for all the support I received from him throughout this project.
Furthermore, I would like to thank Jochen Heinloth, Ludvig Modin, Herman Rohrbach, Pol van Hoften and Torsten Wedhorn for helpful conversations.
Finally I also want to thank the anonymous referee for their valuable feedback, suggesting significant improvements to an earlier version of the manuscript.

This work was funded by the Deutsche Forschungsgemeinschaft (DFG, German Research Foundation), Graduiertenkolleg 2553 and Project-ID 491392403 - TRR 358.