\section{Application to the Siegel modular variety}

As in \Cref{sec:chains-pairs-disps}, $(m, n)$ denotes a tuple of positive integers with $m \geq n + 1$ where we allow $n$ to take the additional value $1 \blank \rdt$.

\subsection{The Siegel modular variety at parahoric level}

Fix a positive integer $g$ and a non-empty subset $J \subseteq \ZZ$ such that $J + 2g \ZZ = J$ and $-J = J$ as in \Cref{subsec:polarized-chains}.
Also fix an auxiliary integer $N \geq 3$ such that $p \nmid N$ and equip $(\ZZ/N\ZZ)^{2g}$ with the standard symplectic form that is represented by the matrix
\[
    \begin{pmatrix}
        0 & \widetilde{I_g} \\ - \widetilde{I_g} & 0
    \end{pmatrix},
    \qquad \text{where} \; \,
    \widetilde{I_g} \coloneqq
    \begin{psmallmatrix}
        & & 1 \\
        & \iddots & \\
        1 & &
    \end{psmallmatrix}.
\]

\begin{definition}
    We define the \emph{Siegel modular variety} as the moduli problem
    \[
        \calA_{g, J, N} \colon \curlybr[\big]{\text{$\Zp$-algebras}} \to \curlybr[\big]{\text{sets}}
    \]
    by setting $\calA_{g, J, N}(R)$ to be the set of isomorphism classes of tuples $((A_i)_i, (\rho_{i, j})_{i, j}, (\lambda_i)_i, \eta)$ that are given as follows.
    \begin{itemize}
        \item
        $((A_i)_i, (\rho_{i, j})_{i, j})$ is a diagram of projective Abelian varieties of dimension $g$ over $R$ of shape $J^{\op}$ such that $\rho_{i, j} \colon A_j \to A_i$ is an isogeny of degree $p^{j - i}$ and $\ker(\rho_{i, i + 2g}) = A_{i + 2g}[p]$.

        \item
        $(\lambda_i)_i \colon ((A_i)_i, (\rho_{i, j})_{i, j}) \to ((A_{- i}^{\vee})_i, (\rho_{-j, -i}^{\vee})_{i, j})$ is a symmetric isomorphism such that for some (or equivalently every) $i \geq 0$ the symmetric isogeny
        \[
            A_i \xrightarrow{\lambda_i} A_{-i}^{\vee} \xrightarrow{\rho_{-i, i}^{\vee}} A_i
        \]
        is a polarization.

        \item
        $\eta \colon A[N] \to \underline{(\ZZ/N\ZZ)^{2g}}$ is an isomorphism of finite étale $R$-group schemes such that there exists an isomorphism $\mu_N \to \underline{\ZZ/N\ZZ}$ making the diagram
        \[
        \begin{tikzcd}
            A[N] \times A[N] \ar[r] \ar[d, "\eta \times \eta"]
            & \mu_N \ar[d]
            \\
            \underline{(\ZZ/N\ZZ)^{2g}} \times \underline{(\ZZ/N\ZZ)^{2g}} \ar[r]
            & \underline{\ZZ/N\ZZ}
        \end{tikzcd}
        \]
        commutative; here $A[N]$ is the $N$-torsion of any of the Abelian varieties $A_i$ (note that $\rho_{i, j}$ restricts to an isomorphism $A_j[N] \to A_i[N]$ because $p$ does not divide $N$) and the upper horizontal arrow is the Weil pairing with respect to $(\lambda_i)_i$.
    \end{itemize}

    We also write $\calA_{g, J, N}^{\wedge}$ for the $p$-completion of $\calA_{g, J, N}$, i.e.\ its restriction to the category of $p$-nilpotent rings.
\end{definition}

\begin{proposition}
    $\calA_{g, J, N}$ is representable by a quasi-projective $\Zp$-scheme.
    Moreover there exists a natural smooth morphism
    \[
        \calA_{g, J, N} \to \squarebr[\Big]{\GSp\roundbr[\big]{(\Lambda_i)_i} \backslash \bbM^{\loc, \GSp}},
    \]
    where we use the notation from \Cref{subsec:quotient-stack}.
\end{proposition}

\begin{proof}
    The representability follows from \cite[Theorem 7.9]{mumford-git}, see also \cite[Definition 6.9]{rapoport-zink-96}.
    For the second claim, see \cite[Section 7.(ii)]{he-rapoport}.
\end{proof}

\begin{definition} \label{def:upsilon}
    We define the morphism $\Upsilon \colon \calA_{g, J, N}^{\wedge} \to \catpolchdisps_{g, J}$ as the composition
    \[
        \Upsilon \colon \calA_{g, J, N}^{\wedge} \to \catpolchpdiv_{g, J} \to \catpolchdisps_{g, J},
    \]
    where the first arrow is given by
    \[
        \roundbr[\big]{(A_i)_i, (\rho_{i, j})_{i, j}, (\lambda_i)_i, \eta} \mapsto \roundbr[\big]{(A_i[p^{\infty}])_i, (\rho_{i, j})_{i, j}, (\lambda_i)_i}
    \]
    (this is well defined because the functor $A \mapsto A[p^{\infty}]$ from projective Abelian varieties to $p$-divisible groups is compatible with dualities up to a sign $-1$, see \cite[Proposition 1.8]{oda}) and the second arrow is the one from \Cref{prop:disp-p-div-chains}.

    We then also have the induced morphism
    \[
        \upsilon^{(m, n)} \colon \calA_{g, J, N}^{\wedge} \to \catpolchdisps_{g, J}^{(m, n)}
    \]
    for $n \neq 1 \blank \rdt$ and
    \[
        \upsilon^{(m, 1 \blank \rdt)} \colon \roundbr[\big]{\calA_{g, J, N}}_{\Fp} \to \catpolchdisps_{g, J}^{(m, 1 \blank \rdt)}.
    \]
\end{definition}

\begin{remark}
    Perfection of the composition
    \[
        \roundbr[\big]{\calA_{g, J, N}}_{\Fp} \xrightarrow{\upsilon^{(m, 1 \blank \rdt)}} \catpolchdisps_{g, J}^{(m, 1 \blank \rdt)} \to \cathpolchdisps_{g, J}^{(m, 1 \blank \rdt)}
    \]
    yields the morphism $\upsilon_K$ from \cite[Section 4.4]{shen-yu-zhang}, at least up to the difference in normalization (see \Cref{rmk:relation-shtukas} and \Cref{rmk:relation-shtukas-2}).

    In particular the fibers of this composition are precisely the EKOR strata defined in \cite[Definition 6.4]{he-rapoport}.
    More precisely we have a natural bijection
    \[
        \abs[\Big]{\roundbr[\big]{\cathpolchdisps^{(m, 1 \blank \rdt)}_{g, J}}_{\Fpbar}} \cong \breve{K}_{\sigma} \backslash (\breve{K}_1 \backslash X),
        \qquad \text{where} \; \,
        X = \bigcup_{w \in \Adm_{g, J}} \breve{K} w \breve{K} \subseteq \GSp(V)(\Qpbrev)
    \]
    as in the introduction.
    For $x \in \breve{K}_{\sigma} \backslash (\breve{K}_1 \backslash X)$ we thus have an associated reduced locally closed substack $\calC_x \subseteq (\cathpolchdisps^{(m, 1 \blank \rdt)}_{g, J})_{\Fpbar}$ that satisfies $\abs{\calC_x} = \curlybr{x}$.
    The locally closed subscheme $\EKOR_x \subseteq (\calA_{g, J, N})_{\Fpbar}$ defined by the pullback square
    \[
    \begin{tikzcd}
        \EKOR_x \ar[r] \ar[d]
        & \roundbr[\big]{\calA_{g, J, N}}_{\Fpbar} \ar[d]
        \\
        \calC_x \ar[r]
        & \roundbr[\Big]{\cathpolchdisps_{g, J}^{(m, 1 \blank \rdt)}}_{\Fpbar}
    \end{tikzcd}
    \]
    then is precisely the EKOR stratum corresponding to $x$, up to possibly carrying some non-reduced structure.

    Below in \Cref{thm:main-res} we will prove that the morphism $\upsilon^{(m, 1 \blank \rdt)}$ is smooth and by \Cref{lem:restricting-pol-hpol} this implies the smoothness of $\calA_{g, J} \to \cathpolchdisps^{(m, n)}_{g, J}$.
    Observing that $\calC_x \to \Spec(\Fpbar)$ is a gerbe, hence smooth, we can then deduce the smoothness of $\EKOR_x$ over $\Fpbar$.
\end{remark}

\subsection{Smoothness of the morphism $\upsilon^{(m, n)}$}

\begin{definition}
    Let $B_g$ be the finite partially ordered set of symmetric Newton polygons starting in $(0, 0)$, ending in $(2g, g)$ and with slopes in $[0, 1]$, where we declare $\nu \leq \nu'$ if $\nu$ lies above $\nu'$.
    This is precisely the set $B(\GSp(V), \mu_g)$, see \cite[Section 2]{he-rapoport} and it classifies isocrystals of height $2g$ and slopes contained in $[0, 1]$ that are furthermore equipped with a symplectic form valued in the standard simple isocrystal of slope $1$, see also \cite[Remark 3.4.(iii)]{rapoport-richartz}.

    We have a natural map of sets $\abs{\catpolchpdiv_{g, J}} \to B_g$ that sends $((X_i)_i, (\rho_{i, j})_{i, j}, (\lambda_i)_i) \in \catpolchpdiv_{g, J}(k)$ for some algebraically closed field $k$ of characteristic $p$ to the Newton polygon of any of the $X_i$.
    We denote the fibers of this map by $S^{\pdiv}_{\nu}$ and their preimages in $\abs{\calA_{g, J, N}^{\wedge}}$ by $S_{\nu}$ and call these subsets \emph{Newton strata}.
\end{definition}

\begin{proposition} \label{thm:newton-strata}
    We have the following properties.
    \begin{itemize}
        \item
        The $S_{\nu} \subseteq \abs{\calA_{g, J, N}^{\wedge}}$ are locally closed subsets.

        \item
        Let $\nu, \nu' \in B_g$.
        Then the intersection $\overline{S_{\nu'}} \cap S_{\nu}$ is non-empty if and only if $\nu \leq \nu'$.

        \item
        $\abs{\catpolchpdiv_{g, J}^{\formal}}$ is the union of those $S^{\pdiv}_{\nu}$ such that $\nu$ does not contain segments of slope $0, 1$.
    \end{itemize}
\end{proposition}

\begin{proof}
    This is all contained in \cite{he-rapoport}.
    See in particular Axiom 3.5, Theorem 5.6 and Section 7.
\end{proof}

\begin{proposition} \label{lem:specializing}
    Let $\nu, \nu' \in B_g$ with $\nu \leq \nu'$ and let $x \in S_{\nu'}^{\pdiv}$.
    Then there exists a preimage $y \in S_{\nu'}$ of $x$ that specializes to a point in $S_{\nu}$.
\end{proposition}

\begin{proof}
    By \Cref{thm:newton-strata} there exists a point in $S_{\nu'}$ specializing to a point in $S_{\nu}$.
    This specialization can be realized by a point
    \[
        A' = \roundbr[\big]{(A'_i)_i, (\rho'_{i, j})_{i, j}, (\lambda'_i)_i, \eta'} \in \calA_{g, J, N}(R)
    \]
    for some rank one valuation ring $R$ of characteristic $p$ with algebraically closed fraction field $K$.
    Write $X' \coloneqq A'[p^{\infty}] \in \catpolchpdiv_{g, J}(R)$ for the image of $A'$.
    After possibly enlarging $R$ we also find an object
    \[
        X = \roundbr[\big]{(X_i)_i, (\rho_{i, j})_{i, j}, (\lambda_i)_i} \in \catpolchpdiv_{g, J}(K)
    \]
    representing $x$.

    Now $X'_K$ and $X$ both lie in $S^{\pdiv}_{\nu'}$, hence there exists an isomorphism between their associated isocrystals that is compatible with the polarizations.
    After multiplying such an isomorphism with $p^M$ for a suitable big enough integer $M \geq 0$ we obtain an isogeny
    \[
        f = (f_i)_i \colon X'_K \to X
    \]
    in $\catchpdiv_{g, J}(K)$ that satisfies $f^* \lambda = p^{2M} \cdot \lambda'$.

    For $i \in J$, let $H_i \subseteq X'_i$ be the flat closure of $\ker(f_i) \subseteq X'_{i, K}$; this is a finite free $R$-subgroup scheme of $X'_i$ of order $p^{2gM}$.
    Define $A''_i \coloneqq A'_i/H_i$ and let $\rho''_{i, j} \colon A''_j \to A''_i$ be the isogeny induced by $\rho'_{i, j}$.
    Then there exists a unique isomorphism $\lambda''_i \colon A''_i \to (A''_{-i})^{\vee}$ that makes the diagram
    \[
    \begin{tikzcd}[column sep = large]
        A'_i \ar[r, "p^{2M} \lambda'_i"] \ar[d]
        & \roundbr[\big]{A'_{-i}}^{\vee}
        \\
        A''_i \ar[r, "\lambda''_i"]
        & \roundbr[\big]{A''_{-i}}^{\vee} \ar[u]
    \end{tikzcd}
    \]
    commutative.
    In this way we obtain a point
    \[
        A'' = \roundbr[\big]{(A''_i)_i, (\rho''_{i, j})_{i, j}, (\lambda''_i)_i, \eta'} \in \calA_{g, J, N}(R)
    \]
    (note that $A''_i[N] = A'_i[N]$ because $H_i$ is $p$-primary).

    By construction $A''$ still realizes a specialization from $S_{\nu'}$ to $S_{\nu}$ and moreover $f$ gives rise to an isomorphism $A''[p^{\infty}]_K \cong X$ in $\catpolchpdiv_{g, J}(K)$.
    Thus setting $y \in S_{\nu'} \subseteq \abs{\calA_{g, J, N}^{\wedge}}$ to be the point corresponding to $A''_K$ finishes the proof.
\end{proof}

\begin{corollary} \label{cor:specializing}
    Every point $x \in \abs{\catpolchpdiv_{g, J}}$ has a preimage $y \in \abs{\calA_{g, J, N}^{\wedge}}$ that specializes to a point mapping into $\abs{\catpolchpdiv_{g, J}^{\formal}}$.
\end{corollary}

\begin{proof}
    This follows from combining \Cref{lem:specializing} with the last part of \Cref{thm:newton-strata}.
\end{proof}

\begin{lemma} \label{lem:infinitesimal-lifting}
    Let $\calX$, $\calY$ be locally Noetherian formal algebraic stacks, let $f \colon \calX \to \calY$ be a morphism representable by algebraic stacks that is locally of finite type, and let $x \in \abs{\calX}$.

    Then $f$ is smooth at $x$ if and only if every lifting problem
    \[
    \begin{tikzcd}
        \Spec(B) \ar[r] \ar[d]
        & \calX \ar[d, "f"]
        \\
        \Spec(B') \ar[r] \ar[ru, dashed]
        & \calY
    \end{tikzcd},
    \]
    where $B' \to B$ is a surjective homomorphism between Artinian local rings and the map $\abs{\Spec(B)} \to \abs{\calX}$ has image $\curlybr{x}$, admits a solution.
\end{lemma}

\begin{proof}
    When $\calX$ and $\calY$ are schemes then this is precisely \cite[Tag 02HX]{stacks-project}.
    The general case can be reduced to this, see also \cite[Tag 0DNV]{stacks-project}.
\end{proof}

\begin{theorem} \label{thm:main-res}
    For $n \neq 1 \blank \rdt$ the morphism $\upsilon^{(m, n)} \colon \calA_{g, J, N}^{\wedge} \to \catpolchdisps_{g, J}^{(m, n)}$ is smooth.
    Similarly also the morphism $\upsilon^{(m, 1 \blank \rdt)} \colon (\calA_{g, J, N})_{\Fp} \to \catpolchdisps_{g, J}^{(m, 1 \blank \rdt)}$ is smooth.
\end{theorem}

\begin{proof}
    Assume that $n \neq 1 \blank \rdt$.
    The morphism $\upsilon^{(m, n)}$ factors as the composition
    \[
        \calA_{g, J, N}^{\wedge} \xrightarrow{\pi} \catpolchpdiv_{g, J} \to \catpolchdisps_{g, J} \to \catpolchdisps_{g, J}^{(m, n)}.
    \]
    We have the following information.
    \begin{itemize}
        \item
        $\pi \colon \calA_{g, J, N}^{\wedge} \to \catpolchpdiv_{g, J}$ is formally étale by the Serre-Tate theorem, see \cite[Appendix]{drinfeld}.

        \item
        $\catpolchpdiv_{g, J} \to \catpolchdisps_{g, J}$ restricts to an isomorphism $\catpolchpdiv_{g, J}^{\formal} \to \catpolchdisps_{g, J}^{F \blank \nilp}$, see \Cref{prop:disp-p-div-chains}.
        As $\catpolchdisps_{g, J}^{F \blank \nilp} \subseteq \catpolchdisps_{g, J}$ is stable under nilpotent thickenings this implies that every lifting problem
        \[
        \begin{tikzcd}
            \Spec(B) \ar[r] \ar[d]
            & \catpolchpdiv_{g, J} \ar[d]
            \\
            \Spec(B') \ar[r] \ar[ru, dashed]
            & \catpolchdisps_{g, J}
        \end{tikzcd},
        \]
        where $B' \to B$ is a surjective homomorphism of $p$-nilpotent rings with nilpotent kernel and the map $\Spec(B) \to \catpolchpdiv_{g, J}$ has image inside $\catpolchpdiv_{g, J}^{\formal}$, admits a (unique) solution.

        \item
        $\catpolchdisps_{g, J} \to \catpolchdisps_{g, J}^{(m, n)}$ is formally smooth, see \Cref{lem:polarized-disps-quotient-stack}.
    \end{itemize}
    Let $U \subseteq \abs{\calA_{g, J, N}^{\wedge}}$ be the smooth locus of $\upsilon^{(m, n)}$.
    Using \Cref{lem:infinitesimal-lifting} we can now deduce the following.
    \begin{itemize}
        \item
        Every point $x \in \abs{\calA_{g, J, N}^{\wedge}}$ that maps into $\abs{\catpolchpdiv_{g, J}^{\formal}}$ is contained in $U$.

        \item
        $U$ is of the form $U = \pi^{-1}(V)$ for some $V \subseteq{\catpolchpdiv_{g, J}}$.
    \end{itemize}
    Now \Cref{cor:specializing} together with the openness of $U \subseteq \abs{\calA_{g, J, N}^{\wedge}}$ implies that $V = \abs{\catpolchpdiv_{g, J}}$ and consequently that $U = \abs{\calA_{g, J, N}^{\wedge}}$ as desired.

    The case $n = 1 \blank \rdt$ now follows from \Cref{lem:disps-quotient-stack}.
\end{proof}