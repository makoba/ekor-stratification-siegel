\section{Categories with actions and dualities}

\subsection{Commutative monoids and actions}

Fix an ambient $(2, 1)$-category $\bsfC$ with finite $2$-products.
We write $\curlybr{\ast} \in \bsfC$ for an empty $2$-product.

\begin{definition}
    A \emph{commutative monoid in $\bsfC$} is an object $\calM \in \bsfC$ that is equipped with $1$-morphisms
    \[
        \otimes \colon \calM \times \calM \to \calM, \qquad 1 \colon \curlybr{\ast} \to \calM
    \]
    and $2$-isomorphisms
    \[
         (a \otimes b) \otimes c \to a \otimes (b \otimes c), \qquad a \otimes b \to b \otimes a, \qquad 1 \otimes a \to a
    \]
    between $1$-morphisms $\calM^i \to \calM$ for $i = 3, 2, 1$ such that the diagrams
    \[
    \begin{tikzcd}[column sep = tiny]
        ((a \otimes b) \otimes c) \otimes d \ar[d, equal] \ar[rr]
        & & (a \otimes (b \otimes c)) \otimes d \ar[rr]
        & & a \otimes ((b \otimes c) \otimes d) \ar[rr]
        & & a \otimes (b \otimes (c \otimes d)) \ar[d, equal]
        \\
        ((a \otimes b) \otimes c) \otimes d \ar[rrr]
        & & & (a \otimes b) \otimes (c \otimes d) \ar[rrr]
        & & & a \otimes (b \otimes (c \otimes d)) 
    \end{tikzcd},
    \]
    \[
    \begin{tikzcd}[column sep = small]
        a \otimes b \ar[rr, equal] \ar[rd]
        & & a \otimes b
        \\
        & b \otimes a \ar[ru] &
    \end{tikzcd},
    \]
    \[
    \begin{tikzcd}
        (a \otimes b) \otimes c \ar[d, equal] \ar[r]
        & (b \otimes a) \otimes c \ar[r]
        & b \otimes (a \otimes c) \ar[r]
        & b \otimes (c \otimes a) \ar[d, equal]
        \\
        (a \otimes b) \otimes c \ar[r]
        & a \otimes (b \otimes c) \ar[r]
        & (b \otimes c) \otimes a \ar[r]
        & b \otimes (c \otimes a)
    \end{tikzcd},
    \]
    \[
    \begin{tikzcd}[column sep = small]
        (1 \otimes a) \otimes b \ar[rr] \ar[rd]
        & & a \otimes b
        \\
        & 1 \otimes (a \otimes b) \ar[ru] &
    \end{tikzcd}
    \]
    are commutative.

    The commutative monoids in $\bsfC$ naturally form the class of objects of a $(2, 1)$-category that is given as follows.

    A $1$-morphism $F \colon \calM \to \calN$ between two commutative monoids in $\bsfC$ is a $1$-morphisms $F \colon \calM \to \calN$ in $\bsfC$ that is equipped with $2$-isomorphisms $F(a \otimes b) \to F(a) \otimes F(b)$, $F(1) \to 1$ such that the diagrams
    \[
    \begin{tikzcd}
        F((a \otimes b) \otimes c) \ar[r] \ar[d]
        & F(a \otimes b) \otimes F(c) \ar[r]
        & (F(a) \otimes F(b)) \otimes F(c) \ar[d]
        \\
        F(a \otimes (b \otimes c)) \ar[r]
        & F(a) \otimes F(b \otimes c) \ar[r]
        & F(a) \otimes (F(b) \otimes F(c))
    \end{tikzcd},
    \]
    \[
    \begin{tikzcd}
        F(a \otimes b) \ar[r] \ar[d]
        & F(a) \otimes F(b) \ar[d]
        \\
        F(b \otimes a) \ar[r]
        & F(b) \otimes F(a)
    \end{tikzcd},
    \]
    \[
    \begin{tikzcd}
        F(1 \otimes a) \ar[r] \ar[rd]
        & F(1) \otimes F(a) \ar[r]
        & 1 \otimes F(a) \ar[ld]
        \\
        & F(a) &
    \end{tikzcd}
    \]
    are commutative.

    A $2$-isomorphism $\alpha \colon F \to G$ between two $1$-morphisms $F, G \colon \calM \to \calN$ as above is a $2$-isomorphism $\alpha \colon F \to G$ in $\bsfC$ such that the digrams
    \[
    \begin{tikzcd}
        F(a \otimes b) \ar[r, "\alpha"] \ar[d]
        & G(a \otimes b) \ar[d]
        \\
        F(a) \otimes F(b) \ar[r, "\alpha \otimes \alpha"]
        & G(a) \otimes G(b)
    \end{tikzcd},
    \]
    \[
    \begin{tikzcd}[column sep = small]
        F(1) \ar[rr, "\alpha"] \ar[rd]
        & & G(1) \ar[ld]
        \\
        & 1 &
    \end{tikzcd}
    \]
    are commutative.
\end{definition}

\begin{definition} \label{def:cat-action}
    Let $\calM$ be a commutative monoid in $\bsfC$.
    An \emph{$\calM$-object in $\bsfC$} is an object $\calC \in \bsfC$ that is equipped with a $1$-morphism
    \[
        \otimes \colon \calM \times \calC \to \calC
    \]
    and $2$-isomorphisms
    \[
        (a \otimes b) \otimes x \to a \otimes (b \otimes x), \qquad 1 \otimes x \to x
    \]
    such that the diagrams
    \[
    \begin{tikzcd}[column sep = tiny]
        ((a \otimes b) \otimes c) \otimes x \ar[d, equal] \ar[rr]
        & & (a \otimes (b \otimes c)) \otimes x \ar[rr]
        & & a \otimes ((b \otimes c) \otimes x) \ar[rr]
        & & a \otimes (b \otimes (c \otimes x)) \ar[d, equal]
        \\
        ((a \otimes b) \otimes c) \otimes x \ar[rrr]
        & & & (a \otimes b) \otimes (c \otimes x) \ar[rrr]
        & & & a \otimes (b \otimes (c \otimes x)) 
    \end{tikzcd},
    \]
    \[
    \begin{tikzcd}[column sep = small]
        (1 \otimes a) \otimes x \ar[rr] \ar[rd]
        & & a \otimes x
        \\
        & 1 \otimes (a \otimes x) \ar[ru] &
    \end{tikzcd}
    \]
    are commutative.

    The tuples $(\calM, \calC)$ consisting of a commutative monoid $\calM$ in $\bsfC$ and an $\calM$-object $\calC$ in $\bsfC$ naturally form the class of objects of a $(2, 1)$-category that is given as follows.

    A $1$-morphism $(F, F') \colon (\calM, \calC) \to (\calN, \calD)$ consists of a $1$-morphisms $F \colon \calM \to \calN$ of commutative monoids in $\bsfC$ and a $1$-morphism $F' \colon \calC \to \calD$ in $\bsfC$ that is equipped with a $2$-isomorphism $F'(a \otimes x) \to F(a) \otimes F'(x)$ such that the diagrams
    \[
    \begin{tikzcd}
        F'((a \otimes b) \otimes x) \ar[r] \ar[d]
        & F(a \otimes b) \otimes F'(x) \ar[r]
        & (F(a) \otimes F(b)) \otimes F'(x) \ar[d]
        \\
        F'(a \otimes (b \otimes x)) \ar[r]
        & F(a) \otimes F'(b \otimes x) \ar[r]
        & F(a) \otimes (F(b) \otimes F'(x))
    \end{tikzcd},
    \]
    \[
    \begin{tikzcd}
        F'(1 \otimes x) \ar[r] \ar[rd]
        & F(1) \otimes F'(x) \ar[r]
        & 1 \otimes F'(x) \ar[ld]
        \\
        & F'(x) &
    \end{tikzcd}
    \]
    are commutative.

    A $2$-isomorphism $(\alpha, \alpha') \colon (F, F') \to (G, G')$ between two $1$-morphisms $(F, F'), (G, G') \colon (\calM, \calC) \to (\calN, \calD)$ as above consists of a $2$-isomorphism $\alpha \colon F \to G$ between $1$-morphisms of commutative monoids in $\bsfC$ and a $2$-isomorphism $\alpha' \colon F' \to G'$ in $\bsfC$ such that the diagram
    \[
    \begin{tikzcd}
        F'(a \otimes x) \ar[r, "\alpha'"] \ar[d]
        & G'(a \otimes x) \ar[d]
        \\
        F(a) \otimes F'(x) \ar[r, "\alpha \otimes \alpha'"]
        & G(a) \otimes G'(x)
    \end{tikzcd}
    \]
    is commutative.
\end{definition}

\subsection{Categories with duality}

\begin{definition} \label{def:cat-duality}
    A \emph{category with duality} is a category $\calC$ that is equipped with an equivalence of categories
    \[
        \calC^{\op} \to \calC, \qquad x \mapsto x^{\vee}
    \]
    and a natural isomorphism
    \[
        x \to x^{\vee \vee}
    \]
    between functors $\calC \to \calC$ such that the diagram
    \[
    \begin{tikzcd}[column sep = small]
        x^{\vee} \ar[rr, equal] \ar[rd]
        & & x^{\vee}
        \\
        & x^{\vee \vee \vee} \ar[ru] &
    \end{tikzcd}
    \]
    is commutative (where the sloped arrow on the right is the image of the isomorphism $x \to x^{\vee \vee}$ under the duality equivalence above).

    A functor $F \colon \calC \to \calD$ between categories with duality is a functor $F \colon \calC \to \calD$ that is equipped with a natural isomorphism $F(x^{\vee}) \to F(x)^{\vee}$ such that the diagram
    \[
    \begin{tikzcd}
        & F(x) \ar[dl] \ar[dr] &
        \\
        F(x^{\vee \vee}) \ar[r]
        & F(x^{\vee})^{\vee} \ar[r]
        & F(x)^{\vee \vee}
    \end{tikzcd}
    \]
    is commutative (where the second horizontal arrow denotes the inverse of the dual of the isomorphism $F(x^{\vee}) \to F(x)^{\vee}$).

    A natural isomorphism $\alpha \colon F \to G$ between two functors $F, G \colon \calC \to \calD$ as above is a natural isomorphism $\alpha \colon F \to G$ such that the diagram
    \[
    \begin{tikzcd}
        F(x^{\vee}) \ar[r, "\alpha"] \ar[d]
        & G(x^{\vee}) \ar[d]
        \\
        F(x)^{\vee} \ar[r, "\alpha^{\vee, -1}"]
        & G(x)^{\vee}
    \end{tikzcd}
    \]
    is commutative.

    In this way the categories with duality naturally form the class of objects of a $(2, 1)$-category.
\end{definition}

\begin{remark} \label{rmk:cat-action-duality}
    The $(2, 1)$-category of (essentially small) categories with duality has finite $2$-products and the natural forgetful $2$-functor
    \[
        \curlybr[\big]{\text{categories with duality}} \to \curlybr[\big]{\text{categories}}
    \]
    preserves finite $2$-products.

    Thus we obtain the notions of a \emph{symmetric monoidal category with duality} and an \emph{action of such a symmetric monoidal category with duality on a category with duality}.
\end{remark}

\begin{remark} \label{rmk:cat-rigid-duality}
    Every rigid symmetric monoidal category naturally carries a duality.
    Thus we obtain a natural $(2, 1)$-functor
    \[
        \curlybr[\big]{\text{rigid symmetric monoidal categories}} \to \curlybr[\big]{\text{symmetric monoidal categories with duality}}.
    \]
\end{remark}

\begin{remark} \label{rmk:cat-lambda}
    Let $\Lambda$ be a commutative ring.
    Then the notions of a category with duality, a symmetric monoidal category (with duality) and an action of a symmetric monoidal category (with duality) on a category (with duality) have obvious $\Lambda$-linear variants that are obtained by requiring all appearing functors to be $\Lambda$-(multi-)linear.
\end{remark}

\begin{definition}
    Let $\calC$ be a $\ZZ$-linear category with duality.
    Then a morphism $f \colon x \to x^{\vee}$ in $\calC$ is called \emph{antisymmetric} if the diagram
    \[
    \begin{tikzcd}[column sep = small]
        x \ar[rr] \ar[rd, swap, "-f"]
        & & x^{\vee \vee} \ar[ld, "f^{\vee}"]
        \\
        & x^{\vee} &
    \end{tikzcd}
    \]
    is commutative.
    We define the groupoid $\catpol(\calC)$ of \emph{polarized objects in $\calC$} as the groupoid of tuples $(x, \lambda)$ with $x \in \calC$ and $\lambda \colon x \to x^{\vee}$ an antisymmetric isomorphism, where isomorphisms $(x, \lambda) \to (y, \zeta)$ are given by isomorphisms $x \to y$ in $\calC$ such that the diagram
    \[
    \begin{tikzcd}
        x \ar[r] \ar[d, "\lambda"]
        & y \ar[d, "\zeta"]
        \\
        x^{\vee} \ar[r]
        & y^{\vee}
    \end{tikzcd}
    \]
    is commutative (where the lower horizontal arrow is the inverse of the dual of $x \to y$).

    Now suppose that $\calC$ is equipped with an action of a $\ZZ$-linear rigid symmetric monoidal category $\calM$ (see \Cref{rmk:cat-rigid-duality}).
    Then a morphism $f \colon x \to a \otimes x^{\vee}$ in $\calC$ with $a \in \calM$ invertible is called \emph{antisymmetric} if the diagram
    \[
    \begin{tikzcd}
        x \ar[r] \ar[rd, swap, "-f"]
        & x^{\vee \vee} \ar[r]
        & a \otimes (a \otimes x^{\vee})^{\vee} \ar[ld, "\id_a \otimes f^{\vee}"]
        \\
        & a \otimes x^{\vee} &
    \end{tikzcd}
    \]
    is commutative.
    In the case $a = 1$ this recovers the first definition above.
    We define the groupoid $\cathpol(\calC) = \cathpol^{\calM}(\calC)$ of \emph{($\calM$-)homogeneously polarized objects in $\calC$} as the groupoid of tuples $(x, a, \lambda)$ with $x \in \calC$, $a \in \calM$ invertible and $\lambda \colon x \to a \otimes x^{\vee}$ an antisymmetric isomorphism, where isomorphisms $(x, a, \lambda) \to (y, b, \zeta)$ are given by tuples of isomorphisms $x \to y$ in $\calC$ and $a \to b$ in $\calM$ such that the diagram
    \[
    \begin{tikzcd}
        x \ar[r] \ar[d, "\lambda"]
        & y \ar[d, "\zeta"]
        \\
        a \otimes x^{\vee} \ar[r]
        & b \otimes y^{\vee}
    \end{tikzcd}
    \]
    is commutative.
\end{definition}

\begin{lemma} \label{lem:pol-hpol-pullback}
    Let $\calC$ be a $\ZZ$-linear category with duality that is equipped with an action of a $\ZZ$-linear rigid symmetric monoidal category $\calM$.
    Then there is a natural $2$-Cartesian diagram of groupoids
    \[
    \begin{tikzcd}
        \catpol(\calC) \ar[r] \ar[d]
        & \cathpol(\calC) \ar[d]
        \\
        \curlybr{\ast} \ar[r]
        & \calM^{\simeq}
    \end{tikzcd},
    \]
    where $\calM^{\simeq}$ denotes the groupoid core of $\calM$.
\end{lemma}

\begin{proof}
    The upper horizontal arrow is given by $(x, \lambda) \mapsto (x, 1, \lambda)$, the right vertical arrow by $(x, a, \lambda) \mapsto a$ and the lower horizontal arrow by $\ast \mapsto 1$.
    The diagram is in fact strictly commutative so that we can use the identity as a commutativity constraint.

    Now the diagram is strictly Cartesian and the right vertical arrow is a fibration (both claims can be checked by a direct computation) so that the diagram is also $2$-Cartesian.
\end{proof}