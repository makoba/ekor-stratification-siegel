\section{Pairs and displays} \label{sec:pairs-disps}

In this section we give recall some of the theory of (not necessarily nilpotent) displays from \cite{zink-02}.
We also develop an analogous theory of $(m, n)$-truncated displays that is inspired by the definition of $(m, n)$-restricted local shtukas given in \cite{xiao-zhu}.

The letter $R$ denotes a $p$-nilpotent ring and $(m, n)$ denotes a tuple of positive integers with $m \geq n + 1$.

\subsection{Witt vectors} \label{subsec:witt-vectors}

We use the following notation concerning Witt vectors.

\begin{itemize}
    \item
    We write $W(R)$ for the ring of \emph{($p$-typical) Witt vectors of $R$} and $I_R \subseteq W(R)$ for its \emph{augmentation ideal}, i.e.\ the kernel of the projection $W(R) \to R$.

    Similarly we write $W_n(R)$ for the ring of $n$-truncated Witt vectors of $R$ and $I_{n, R} \subseteq W_n(R)$ for its augmentation ideal.

    \item
    We denote the \emph{Witt vector Frobenius} on $W(R)$ by $\sigma \colon W(R) \to W(R)$.
    Given a $W(R)$-module $M$ we write $M^{\sigma} \coloneqq W(R) \otimes_{\sigma, W(R)} M$ for its Frobenius twist.

    Note that $\sigma$ induces a ring homomorphism $\sigma \colon W_m(R) \to W_n(R)$ on truncated Witt vectors (here it is crucial that $m \geq n + 1$, at least when $R$ is not of characteristic $p$).

    When $R$ is of characteristic $p$ we also write $\sigma \colon R \to R$ for the $p$-power Frobenius on $R$.


    \item
    We write
    \[
        W(R)^{\sigma = \id} \coloneqq \set[\big]{x \in W(R)}{\sigma(x) = x} \subseteq W(R)
    \]
    for the subring of $\sigma$-invariant elements.
    Note that we have a natural isomorphism
    \[
        \Zp(R) \coloneqq \Cont \roundbr[\big]{\abs{\Spec(R)}, \Zp} \to W(R)^{\sigma = \id};
    \]
    when $R$ is of characteristic $p$ this follows from the chain of identifications
    \[
        \Zp(R) \cong W \roundbr[\big]{\Fp(R)} \cong W \roundbr[\big]{R^{\sigma = \id}} \cong W(R)^{\sigma = \id}
    \]
    and the general case follows because the reduction morphism $\Zp(R) \to \Zp(R/pR)$ is an isomorphism and the $\sigma$-stable ideal $\ker(W(R) \to W(R/pR)) \subseteq W(R)$ is killed by a power of $\sigma$ so that every $\sigma$-invariant element $x \in W(R/pR)$ lifts uniquely to a $\sigma$-invariant element $\widetilde{x} \in W(R)$.

    Similarly we write
    \[
        W_m(R)^{\sigma = \id} \coloneqq \set[\Big]{x \in W_m(R)}{\sigma(x) = x \in W_n(R)} \subseteq W_m(R).
    \]
    Note that this is slightly ambiguous as the chosen $n$ does not appear in the notation; however it should always be clear from the context what $n$ is used.

    \item
    Recall that the inverse of the \emph{Verschiebung} is a $\sigma$-linear map $I_R \to W(R)$.
    We denote its linearization by $\sigma^{\divd} \colon I_R^{\sigma} \to W(R)$ and call it the \emph{divided Frobenius}.
    For $x \in I_R$ we have $p \cdot \sigma^{\divd}(1 \otimes x) = \sigma(x)$, justifying the name.

    We also have a truncated variant of the divided Frobenius $\sigma^{\divd} \colon W_n(R) \otimes_{\sigma, W_m(R)} I_{m, R} \to W_n(R)$.
    
    \item
    Let $M$, $N$ be finite projective $W(R)$-modules and let $f \colon M \to I_R N \subseteq N$ be a $W(R)$-linear map.
    We write $f^{\sigma, \divd}$ for the composition
    \[
        f^{\sigma, \divd} \colon M^{\sigma} \xrightarrow{f^{\sigma}} I_R^{\sigma} \otimes_{W(R)} N^{\sigma} \xrightarrow{\sigma^{\divd} \otimes \id} N^{\sigma}.
    \]
    Then we have $p \cdot f^{\sigma, \divd} = f^{\sigma}$ and given homomorphisms of finite projective $W(R)$-modules $g \colon L \to M$ and $h \colon N \to P$ we have $(f \circ g)^{\sigma, \divd} = f^{\sigma, \divd} \circ g^{\sigma}$ and $(h \circ f)^{\sigma, \divd} = h^{\sigma} \circ f^{\sigma, \divd}$.

    Similarly, given finite projective $W_m(R)$-modules $M$ and $N$ and a homomorphism of $W_m(R)$-modules $f \colon M \to I_{m, R} N \subseteq N$ we write $f^{\sigma, \divd}$ for the composition
    \begin{align*}
        f^{\sigma, \divd} \colon W_n(R) \otimes_{\sigma, W_m(R)} M &\xrightarrow{f^{\sigma}} \roundbr[\big]{W_n(R) \otimes_{\sigma, W_m(R)} I_{m, R}} \otimes_{W_n(R)} \roundbr[\big]{W_n(R) \otimes_{\sigma, W_m(R)} N} \\
        & \xrightarrow{\sigma^{\divd} \otimes \id} W_n(R) \otimes_{\sigma, W_m(R)} N.
    \end{align*}
    This construction has similar properties as the non-truncated version.

    \item
    Given a smooth affine $\Zp$-group scheme $G$ we write $L^+ G$ for the \emph{(Witt vector) positive loop group of $G$}, i.e.\ the flat affine $\Zp$-group scheme given by $(L^+ G)(R) = G(W(R))$ (see also \cite[Section 2.2]{bueltel-hedayatzadeh}).

    We also write $L^{(n)} G$ for the $n$-truncated positive loop group of $G$, i.e.\ the smooth affine $\Zp$-group scheme given by $(L^{(n)} G)(R) = G(W_n(R))$.
\end{itemize}

We also record the following technical lemma that will be used multiple times throughout the article.

\begin{lemma} \label{lem:chains-rank}
    Suppose that we are given an admissible linearly topologized ring $A$ (see \cite[Tag 07E8]{stacks-project}) such that $p$ is topologically nilpotent in $A$.
    Let $M$, $M'$ be finite projective $A$-modules of rank $h$ and let $f \colon M \to M'$ and $g \colon M' \to M$ be morphisms of $A$-modules such that $g \circ f = p \cdot \id_M$ and $f \circ g = p \cdot \id_{M'}$.
    Suppose furthermore that there exist integers $\ell$ and $\ell'$ with $\ell + \ell' = h$ such that for every continuous ring homomorphism $A \to k$ with $k$ an algebraically closed field the induced homomorphisms of $k$-vector spaces
    \[
        f \colon k \otimes_A M \to k \otimes_A M' \quad \text{and} \quad g \colon k \otimes_A M' \to k \otimes_A M
    \]
    are of rank $\ell$ and $\ell'$.

    Then the induced homomorphisms of $A/pA$-modules
    \[
        f \colon M/pM \to M'/pM' \quad \text{and} \quad g \colon M'/pM' \to M/pM
    \]
    are of constant rank $\ell$ and $\ell'$ in the sense that their respective image is a direct summand (hence finite projective) of the indicated rank.
\end{lemma}

\begin{proof}
    Let $\fraka \subseteq A$ be an ideal of definition that contains $p$.
    Note that the condition that $f$ and $g$ are of constant rank $\ell$ and $\ell'$ is representable by a finitely presented locally closed subscheme $Z \subseteq \Spec(A/\fraka)$.
    By assumption we have $\abs{Z} = \abs{\Spec(A/\fraka)}$ so that $Z$ is the vanishing locus of a nilpotent ideal $\frakb \subseteq A/\fraka$.
    After replacing $\fraka$ by the preimage of $\frakb$ under $A \to A/\fraka$ we thus may assume that the homomorphisms of $A/\fraka$-modules
    \[
        f \colon M/\fraka M \to M'/\fraka M' \quad \text{and} \quad g \colon M'/\fraka M' \to M/\fraka M
    \]
    are of constant rank $\ell$ and $\ell'$.

    Modulo $\fraka$ the morphisms $f$ and $g$ now compose to $0$ in both directions and are of complementary constant rank.
    Thus there exist direct sum decompositions
    \[
        M/\fraka M = P \oplus P' \quad \text{and} \quad M'/\fraka M' = P \oplus P'
    \]
    with respect to which we have $f = \begin{psmallmatrix} \id_P & 0 \\ 0 & 0 \end{psmallmatrix}$ and $g = \begin{psmallmatrix} 0 & 0 \\ 0 & \id_{P'} \end{psmallmatrix}$.
    Lift these decompositions to
    \[
        M = Q \oplus Q' \quad \text{and} \quad M' = Q \oplus Q'.
    \]
    After possibly modifying the lifts we may then assume that
    \[
        f = \begin{pmatrix} \id_Q & 0 \\ 0 & \ast \end{pmatrix} \quad \text{and} \quad g = \begin{pmatrix} \ast & \ast \\ \ast & \id_{Q'} \end{pmatrix}.
    \]
    The assumptions $g \circ f = p \cdot \id_M$ and $f \circ g = p \cdot \id_{M'}$ then imply that we in fact have
    \[
        f = \begin{pmatrix} \id_Q & 0 \\ 0 & p \cdot \id_{Q'} \end{pmatrix} \quad \text{and} \quad g = \begin{pmatrix} p \cdot \id_Q & 0 \\ 0 & \id_{Q'} \end{pmatrix}
    \]
    so that the claim follows.
\end{proof}

\subsection{Pairs and displays} \label{subsec:pairs-displays}

Let $h$ and $d$ denote integers with $0 \leq d \leq h$.

\begin{definition}
    A \emph{pair (of type $(h, d)$) over $R$} is a tuple $(M, M_1)$ consisting of a finite projective $W(R)$-module $M$ (of rank $h$) and a $W(R)$-submodule $M_1 \subseteq M$ with $I_R M \subseteq M_1$ and such that $M_1/I_R M \subseteq M/I_R M$ is a direct summand (of rank $d$).

    An \emph{$m$-truncated pair over $R$} is a tuple $(M, M_1)$ consisting of a finite projective $W_m(R)$-module $M$ and a $W_m(R)$-submodule $M_1 \subseteq M$ with $I_{m, R} M \subseteq M_1$ and such that $M_1/I_{m, R} M \subseteq M/I_{m, R} M$ is a direct summand.
\end{definition}

\begin{definition}
    Let $(M, M_1)$ and $(M', M'_1)$ be two pairs over $R$.
    Then a \emph{morphism} $f \colon (M, M_1) \to (M', M'_1)$ is a morphism of $W(R)$-modules $f \colon M \to M'$ such that $f(M_1) \subseteq M'_1$.

    In the same way we also define morphisms of $m$-truncated pairs.
\end{definition}

\begin{remark} \label{rmk:base-changing-pairs}
    Let $(M, M_1)$ be a pair over $R$ and let $R \to R'$ be a morphism of $p$-complete rings.
    Then we can form the base change $(M', M'_1) = (M, M_1)_{R'}$ that is a pair over $R'$.
    It is characterized by
    \[
        M' = W(R') \otimes_{W(R)} M \quad \text{and} \quad M'_1/I_{R'} M' = R' \otimes_R (M_1/I_R M).
    \]
    Similarly we can also base change $m$-truncated pairs.

    From the descent result \cite[Corollary 34]{zink-02} it follows that the assignment $R \mapsto \curlybr{\text{pairs over $R$}}$ defines a stack of $W(\calO_{\Spf(\Zp)})$-linear categories for the fpqc topology; here $W(\calO_{\Spf(\Zp)})$ denotes the sheaf of rings that is given by $R \mapsto W(R)$.

    Similarly, using \cite[Lemma 3.12]{lau-10}, we see that the assignment $R \mapsto \curlybr{\text{$m$-truncated pairs over $R$}}$ defines a stack of $W_m(\calO_{\Spf(\Zp)})$-linear categories.
\end{remark}

\begin{remark} \label{rmk:truncating-pairs}
    There are natural truncation functors
    \[
        \curlybr[\big]{\text{pairs over $R$}} \to \curlybr[\big]{\text{$m$-truncated pairs over $R$}}
    \]
    and
    \[
        \curlybr[\big]{\text{$m'$-truncated pairs over $R$}} \to \curlybr[\big]{\text{$m$-truncated pairs over $R$}}
    \]
    for $m \leq m'$.
\end{remark}

\begin{lemma}
    Let $(M, M_1)$ be a pair over $R$.
    Then $(M, M_1)$ has a \emph{normal decomposition} $(L, T)$, i.e.\ a direct sum decomposition $M = L \oplus T$ such that $M_1 = L \oplus I_R T$.
    Given a second pair $(M', M'_1)$ with normal decomposition $(L', T')$, every morphism of pairs $f \colon (M, M_1) \to (M', M'_1)$ can be written in matrix form $f = \begin{psmallmatrix} a & b \\ c & d \end{psmallmatrix}$ with
    \[
        a \colon L \to L', \quad b \colon T \to L', \quad c \colon L \to I_R T', \quad d \colon T \to T'.
    \]

    The same is true for $m$-truncated pairs.
\end{lemma}

\begin{proof}
    Set $L' \coloneqq M_1/I_R M \subseteq M/I_R M$.
    By definition it is a direct summand so that we can choose a complement $T' \subseteq M/I_R M$.
    As $W(R)$ is Henselian along $I_R$ we can now lift the decomposition $M/I_R M = L' \oplus T'$ to a decomposition $M = L \oplus T$ as desired.
    The claim about writing morphisms as matrices with respect to chosen normal decompositions is immediate.
\end{proof}

\begin{definition} \label{def:m-1-tilde}
    We define a natural $\sigma$-linear functor
    \[
    \begin{array}{r c l}
        \curlybr[\Big]{\text{pairs (of type $(h, d)$) over $R$}}
        & \to
        & \curlybr[\Bigg]{\begin{gathered}\text{finite projective $W(R)$-modules} \\ \text{(of rank $h$)}\end{gathered}}, \\[5mm]
        (M, M_1)
        & \mapsto
        & \widetilde{M_1}
    \end{array}
    \]
    as follows.
    \begin{itemize}
        \item
        Given a pair $(M, M_1)$ over $R$ with normal decomposition $(L, T)$, we set $\widetilde{M_1} \coloneqq L^{\sigma} \oplus T^{\sigma}$.

        \item
        Given two pairs $(M, M_1)$ and $(M', M'_1)$ with normal decompositions $(L, T)$ and $(L', T')$ and a morphism $f = \begin{psmallmatrix} a & b \\ c & d \end{psmallmatrix} \colon (M, M_1) \to (M', M'_1)$ we define
        \[ 
            \widetilde{f} \coloneqq \begin{pmatrix} a^{\sigma} & p \cdot b^{\sigma} \\ c^{\sigma, \divd} & d^{\sigma} \end{pmatrix} \colon \widetilde{M_1} \to \widetilde{M'_1}.
        \]
    \end{itemize}

    In the same way we also define a natural functor
    \[
    \begin{array}{r c l}
        \curlybr[\big]{\text{$m$-truncated pairs over $R$}}
        & \to
        & \curlybr[\big]{\text{finite projective $W_n(R)$-modules}}, \\[1mm]
        (M, M_1)
        & \mapsto
        & \widetilde{M_1}.
    \end{array}
    \]
\end{definition}

\begin{remark} \label{rmk:m-1-tilde}
    We make the following remarks.
    \begin{itemize}
        \item
        Checking that the functor $(M, M_1) \mapsto \widetilde{M_1}$ from \Cref{def:m-1-tilde} is well-defined amounts to checking that the definition of $\widetilde{f}$ is compatible with identities and composition.
        This can be easily verified using the properties of $(\blank)^{\sigma, \divd}$, see \Cref{subsec:witt-vectors}.

        The definition $\widetilde{f} \coloneqq \begin{psmallmatrix} a^{\sigma} & p \cdot b^{\sigma} \\ c^{\sigma, \divd} & d^{\sigma} \end{psmallmatrix}$ imitates the expression $\begin{psmallmatrix} 1 & 0 \\ 0 & p \end{psmallmatrix}^{-1} \begin{psmallmatrix} a & b \\ c & d \end{psmallmatrix}^{\sigma} \begin{psmallmatrix} 1 & 0 \\ 0 & p \end{psmallmatrix}$; the latter a priori only makes sense after base changing to $W(R)[1/p]$ but then both terms agree.

        \item
        Let $(M, M_1)$ be a pair over $R$.
        Then we can informally think of $\widetilde{M_1}$ as the \enquote{correct version} of the Frobenius twist $M_1^{\sigma}$ that usually fails to be a finite projective $W(R)$-module.

        We have a natural surjective $W(R)$-linear map
        \[
            M_1^{\sigma} \cong L^{\sigma} \oplus \roundbr[\big]{I_R^{\sigma} \otimes_{W(R)} T^{\sigma}} \xrightarrow{(\id, \sigma^{\divd} \otimes \id)} L^{\sigma} \oplus T^{\sigma} \cong \widetilde{M_1}
        \]
        and this map actually is an isomorphism when $R$ is a perfect ring of characteristic $p$.

        We also have natural $W(R)$-linear maps
        \[
            \widetilde{M_1} \cong L^{\sigma} \oplus T^{\sigma} \xrightarrow{\begin{psmallmatrix} 1 & 0 \\ 0 & p \end{psmallmatrix}} L^{\sigma} \oplus T^{\sigma} \cong M^{\sigma}
            \quad \text{and} \quad
            M^{\sigma} \cong L^{\sigma} \oplus T^{\sigma} \xrightarrow{\begin{psmallmatrix} p & 0 \\ 0 & 1 \end{psmallmatrix}} L^{\sigma} \oplus T^{\sigma} \cong \widetilde{M_1}
        \]
        that we can informally think of as the \enquote{inclusion} and the \enquote{multiplication-by-$p$-map} respectively.
        Similar maps also exist in the truncated situation.

        \item
        The theory of \emph{higher displays} as developed in \cite{lau-higher-frames} and \cite{daniels-tannakian-displays} gives a way to conceptualize \Cref{def:m-1-tilde}.

        Let $W(R)^{\oplus}$ be the $\ZZ$-graded ring underlying the Witt frame (see \cite[Definition 2.2]{daniels-tannakian-displays}) and recall that it is equipped with two ring homomorphisms $\tau, \sigma \colon W(R)^{\oplus} \to W(R)$.
        Now giving a pair $(M, M_1)$ over $R$ is equivalent to giving a finite projective graded $W(R)^{\oplus}$-module whose type is concentrated in $[0, 1]$ and the functors $(M, M_1) \mapsto M$ and $(M, M_1) \mapsto \widetilde{M_1}$ correspond to base changing along $\tau$ and $\sigma$ respectively.
    \end{itemize}
\end{remark}

\begin{definition} \label{def:disps}
    A \emph{display over $R$} is a tuple $(M, M_1, \Psi)$ where $(M, M_1)$ is a pair over $R$ and $\Psi \colon \widetilde{M_1} \to M$ is an isomorphism of $W(R)$-modules.
    We call $\Psi$ the \emph{divided Frobenius} of the display.

    Similarly, an \emph{$(m, n)$-truncated display over $R$} is a tuple $(M, M_1, \Psi)$ where $(M, M_1)$ is an $m$-truncated pair over $R$ and $\Psi \colon \widetilde{M_1} \to W_n(R) \otimes_{W_m(R)} M$ is an isomorphism of $W_n(R)$-modules.
\end{definition}

\begin{remark}
    Similarly as was explained for pairs in \Cref{rmk:base-changing-pairs} and \Cref{rmk:truncating-pairs} one can also base change and truncate displays.

    The assignment $R \mapsto \curlybr{\text{displays over $R$}}$ defines an fpqc-stack of $\underline{\Zp}$-linear categories; here $\underline{\Zp}$ denotes the sheaf that is given by $R \mapsto \Zp(R)$.

    Similarly the assignment $R \mapsto \curlybr{\text{$(m, n)$-truncated displays over $R$}}$ defines a stack of $W_m(\calO_{\Spf(\Zp)})^{\sigma = \id}$-linear categories.
\end{remark}

\begin{definition} \label{def:frobenius}
    Let $(M, M_1, \Psi)$ be a display over $R$.
    We define the \emph{Frobenius of $(M, M_1, \Psi)$} as the composition
    \[
        F_M \colon M^{\sigma} \to \widetilde{M_1} \xrightarrow{\Psi} M,
    \]
    where the first arrow is the \enquote{multiplication-by-$p$-map} introduced in \Cref{rmk:m-1-tilde}.
    Furthermore we say that $(M, M_1, \Psi)$ is \emph{$F$-nilpotent} if there exists some $N \in \ZZ_{\geq 0}$ such that the $R/pR$-module homomorphism
    \[
        R/pR \otimes \roundbr[\big]{F_M \circ \dotsb \circ F_M^{\sigma^{N - 1}}} \colon R/pR \otimes_{W(R)} M^{\sigma^N} \to R/pR \otimes_{W(R)} M
    \]
    vanishes.

    Now let $(M, M_1, \Psi)$ be an $(m, n)$-truncated display over $R$.
    Then we again define the \emph{Frobenius of $(M, M_1, \Psi)$} as the composition
    \[
        F_M \colon W_n(R) \otimes_{\sigma, W_m(R)} M \to \widetilde{M_1} \xrightarrow{\Psi} W_n(R) \otimes_{W_m(R)} M
    \]
    and say that $(M, M_1, \psi)$ is \emph{$F$-nilpotent} if there exists some $N \in \ZZ_{\geq 0}$ such that the $R/pR$-module homomorphism
    \[
        \roundbr[\big]{R/pR \otimes_{W_n(R)} F_M} \circ \dotsb \circ \roundbr[\big]{R/pR \otimes_{W_n(R)} F_M}^{(p^{N - 1})} \colon \roundbr[\big]{R/pR \otimes_{W_m(R)} M}^{(p^N)} \to R/pR \otimes_{W_m(R)} M
    \]
    vanishes.
\end{definition}

\begin{remark}
    It follows immediately from the definition that an ($(m, n)$-truncated) display over $R$ is $F$-nilpotent if and only if its $(2, 1)$-truncated base change to $R/pR$ is $F$-nilpotent.
\end{remark}

\begin{remark}[Relation with Dieudonné modules] \label{rmk:relation-dieudonne}
    Suppose that $R$ is perfect of characteristic $p$.
    Then there is the classical notion of a \emph{Dieudonné module over $R$}.

    Such a Dieudonné module over $R$ is a tuple $(M, F_M)$ consisting of a finite projective $W(R)$-module $M$ and an isomorphism $F_M \colon M^{\sigma}[1/p] \to M[1/p]$ that satisfies $p M \subseteq F_M(M^{\sigma}) \subseteq M$.

    We have a natural equivalence of categories
    \[
        \curlybr[\big]{\text{displays over $R$}} \to \curlybr[\big]{\text{Dieudonné modules over $R$}}
    \]
    that sends a display $(M, M_1, \Psi)$ to $(M, F_M)$ where $F_M$ is the Frobenius from \Cref{def:frobenius}.
    The inverse of this equivalence is given by sending $(M, F_M)$ to $(M, M_1, \Psi)$, where
    \[
        M_1 \coloneqq p \cdot F_M^{-1}(M)^{\sigma^{-1}} \subseteq M
        \quad \text{and} \quad
        \Psi \colon \widetilde{M_1} \cong p \cdot F_M^{-1}(M) \xrightarrow{p^{-1} \cdot F_M} M.
    \]
    Moreover the display corresponding to a Dieudonné module $(M, F_M)$ over $R$ is of type $(h, d)$ if and only if $M$ is a finite projective $W(R)$-module of rank $h$ and $M/F_M(M^{\sigma})$ is a finite projective $R$-module of rank $d$.
\end{remark}

\begin{remark}[Relation with truncated displays in the sense of Lau and Zink] \label{rmk:relation-lau-zink}
    Let us recall the notions of \emph{truncated pairs and displays} from \cite{lau-zink-18}, see also \cite{lau-10}.
    
    Consider the ring
    \[
        \calW_n(R) \coloneqq W_{n + 1}(R)/ \roundbr[\big]{0, \dotsc, 0, R[p]},
    \]
    where $R[p] \subseteq R$ denotes the $p$-torsion in $R$.
    We have $(0, \dotsc, 0, R[p]) \cdot I_{n + 1, R} = 0$ so that $I_{n + 1, R}$ is naturally a $\calW_n(R)$-module and we have a Frobenius $\sigma \colon \calW_n(R) \to W_n(R)$ as well as a divided Frobenius $\sigma^{\divd} \colon W_n(R) \otimes_{\sigma, \calW_n(R)} I_{n + 1, R} \to W_n(R)$.

    We now have the $\calW_n(R)$-linear category of \emph{$n$-truncated Lau-Zink-pairs} that has the following description.
    Every $n$-truncated Lau-Zink-pair (that we informally denote by $(M, M_1)$) has a normal decomposition $(L, T)$ where $L$ and $T$ are finite projective $\calW_n(R)$-modules and given two such objects $(M, M_1)$ and $(M', M'_1)$ with normal decompositions $(L, T)$ and $(L', T')$, morphisms $f \colon (M, M_1) \to (M', M'_1)$ are given by matrices $f = \begin{psmallmatrix} a & b \\ c & d \end{psmallmatrix}$ with
    \[
        a \colon L \to L', \quad
        b \colon T \to L', \quad
        c \colon L \to I_{n + 1, R} \otimes_{\calW_n(R)} T', \quad
        d \colon T \to T'.
    \]

    There again is a $\sigma$-linear functor
    \[
        \curlybr[\big]{\text{$n$-truncated Lau-Zink-pairs over $R$}} \to \curlybr[\big]{\text{finite projective $W_n(R)$-modules}}, \qquad (M, M_1) \mapsto \widetilde{M_1}
    \]
    that sends an $n$-truncated Lau-Zink-pair $(M, M_1)$ with normal decomposition $(L, T)$ to
    \[
        \widetilde{M_1} \coloneqq \roundbr[\big]{W_n(R) \otimes_{\sigma, \calW_n(R)} L} \oplus \roundbr[\big]{W_n(R) \otimes_{\sigma, \calW_n(R)} T}
    \]
    and a morphism $f = \begin{psmallmatrix} a & b \\ c & d \end{psmallmatrix}$ as above to $\widetilde{f} \coloneqq \begin{psmallmatrix} a^{\sigma} & p \cdot b^{\sigma} \\ c^{\sigma, \divd} & d^{\sigma} \end{psmallmatrix}$ similar as in \Cref{def:m-1-tilde}.
    An \emph{$n$-truncated Lau-Zink-display} is then defined to be a tuple $(M, M_1, \Psi)$ where $(M, M_1)$ is an $n$-truncated Lau-Zink-pair and $\Psi \colon \widetilde{M_1} \to W_n(R) \otimes_{\calW_n(R)} M$ is an isomorphism.

    From this description it is clear that there exist natural truncation functors
    \[
        \curlybr[\big]{\text{$(m', n')$-truncated displays over $R$}} \to \curlybr[\big]{\text{$n$-truncated Lau-Zink-displays over $R$}}
    \]
    for $n \leq n'$ and
    \[
        \curlybr[\big]{\text{$n'$-truncated Lau-Zink-displays over $R$}} \to \curlybr[\big]{\text{$(m, n)$-truncated displays over $R$}}
    \]
    for $m \leq n'$.

    Let us remark that for $R$ of characteristic $p$ we have an equivalence
    \[
        \curlybr[\big]{\text{$1$-truncated Lau-Zink-displays over $R$}} \to \curlybr[\big]{\text{$F$-zips over $R$}},
    \]
    where the right hand side denotes the the category of $F$-Zips from \cite[Definition 1.5]{moonen-wedhorn} with type concentrated in $[0, 1]$, see \cite[Example 3.6.4]{lau-higher-frames}.
\end{remark}

\subsection{Duals and twists} \label{subsec:duals-twists}

\begin{definition}
    Let $(M, M_1)$ be a pair over $R$.
    Then we define its \emph{dual}
    \[
        (M, M_1)^{\vee} \coloneqq (M^{\vee}, M_1^{\ast})
    \]
    as follows.
    \begin{itemize}
        \item
        $M^{\vee} = \Hom_{W(R)}(M, W(R))$ is the dual of the finite projective $W(R)$-module $M$.

        \item
        $M_1^{\ast} \subseteq M$ is the $W(R)$-submodule of all $\omega \colon M \to W(R)$ such that $\omega(M_1) \subseteq I_R$.
        Equivalently it is the preimage under
        \[
            M^{\vee} \to M^{\vee}/I_R M^{\vee} \cong (M/I_R M)^{\vee}
        \]
        of the orthogonal complement $(M_1/I_R M)^{\perp} \subseteq (M/I_R M)^{\vee}$.
    \end{itemize}
    This endows the category of pairs over $R$ with a $W(R)$-linear duality in the sense of \Cref{def:cat-duality} and \Cref{rmk:cat-lambda}.

    We similarly define duals of $m$-truncated pairs.
\end{definition}

\begin{remark}
    Note that if $(M, M_1)$ is an ($m$-truncated) pair of type $(h, d)$ over $R$ then its dual $(M, M_1)^{\vee}$ is of type $(h, h - d)$.
\end{remark}

\begin{lemma} \label{lem:m-1-tilde-dual}
    The functor
    \[
        \curlybr[\big]{\text{pairs over $R$}} \to \curlybr[\big]{\text{finite projective $W(R)$-modules}}, \qquad (M, M_1) \mapsto \widetilde{M_1}
    \]
    is naturally compatible with dualities.

    The same is true for the functor
    \[
        \curlybr[\big]{\text{$m$-truncated pairs over $R$}} \to \curlybr[\big]{\text{finite projective $W_n(R)$-modules}}, \qquad (M, M_1) \mapsto \widetilde{M_1}.
    \]
\end{lemma}

\begin{proof}
    Given a pair $(M, M_1)$ over $R$ we need to define a natural isomorphism $\widetilde{M_1^{\ast}} \to \widetilde{M_1}^{\vee}$.
    Let $(L, T)$ be a normal decomposition of $(M, M_1)$.
    Then $(T^{\vee}, L^{\vee})$ is a normal decomposition of $(M, M_1)^{\vee}$ and we can define the desired isomorphism as
    \[
        \begin{pmatrix} 0 & \id_{L^{\sigma, \vee}} \\ \id_{T^{\sigma, \vee}} & 0 \end{pmatrix} \colon \widetilde{M_1^{\ast}} \cong T^{\vee, \sigma} \oplus L^{\vee, \sigma} \to \roundbr[\big]{L^{\sigma} \oplus T^{\sigma}}^{\vee} \cong \widetilde{M_1}^{\vee}. \qedhere
    \]
\end{proof}

\begin{definition}
    Let $(M, M_1, \Psi)$ be a display over $R$.
    Then we define its \emph{dual}
    \[
        (M, M_1, \Psi)^{\vee} \coloneqq (M^{\vee}, M_1^{\ast}, \Psi^{\vee, -1}),
    \]
    where we implicitly use \Cref{lem:m-1-tilde-dual} to make sense of $\Psi^{\vee, -1}$ as an isomorphism $\widetilde{M_1^{\ast}} \to M^{\vee}$.
    This endows the category of displays over $R$ with a $\Zp(R)$-linear duality.

    We similarly define duals of $(m, n)$-truncated displays.
\end{definition}

\begin{definition}
    Let $(M, M_1)$ be a pair over $R$ and let $I$ be an finite projective $W(R)$-module.
    Then we define the \emph{twist}
    \[
        I \otimes (M, M_1) \coloneqq (I \otimes_{W(R)} M, I \otimes_{W(R)} M_1).
    \]
    This defines an action of the symmetric monoidal category of finite projective $W(R)$-modules on the category of pairs over $R$ in the sense of \Cref{def:cat-action}.

    We similarly define twists of $m$-truncated pairs over $R$ by finite projective $W_m(R)$-modules.
\end{definition}

\begin{remark}
    In fact the symmetric monoidal category of finite projective $W(R)$-modules is rigid and $W(R)$-linear and the action defined above is naturally compatible with dualities and the $W(R)$-linear structure, see \Cref{rmk:cat-action-duality}, \Cref{rmk:cat-rigid-duality} and \Cref{rmk:cat-lambda}.

    In the same way also the action in the truncated setting is naturally compatible with dualities and the $W_m(R)$-linear structure.
\end{remark}

\begin{lemma} \label{lem:m-1-tilde-twist}
    The functor
    \[
        \curlybr[\big]{\text{pairs over $R$}} \to \curlybr[\big]{\text{finite projective $W(R)$-modules}}, \qquad (M, M_1) \mapsto \widetilde{M_1}
    \]
    is naturally equivariant with respect to the symmetric monoidal functor
    \[
        \curlybr[\big]{\text{finite projective $W(R)$-modules}} \to \curlybr[\big]{\text{finite projective $W(R)$-modules}}, \qquad I \mapsto I^{\sigma}
    \]
    in the sense of \Cref{def:cat-action} and \Cref{rmk:cat-action-duality}.

    Similarly the functor
    \[
        \curlybr[\big]{\text{$m$-truncated pairs over $R$}} \to \curlybr[\big]{\text{finite projective $W_n(R)$-modules}}, \qquad (M, M_1) \mapsto \widetilde{M_1}
    \]
    is naturally equivariant with respect to the symmetric monoidal functor
    \[
        \curlybr[\big]{\text{finite projective $W_m(R)$-modules}} \to \curlybr[\big]{\text{finite projective $W_n(R)$-modules}}, \qquad I \mapsto W_n(R) \otimes_{\sigma, W_m(R)} I.
    \]
\end{lemma}

\begin{proof}
    Given a pair $(M, M_1)$ over $R$ and a finite projective $W(R)$-module $I$ we need to define a natural isomorphism $(I \otimes_{W(R)} M_1)^{\sim} \to I^{\sigma} \otimes_{W(R)} \widetilde{M_1}$.
    Let $(L, T)$ be a normal decomposition of $(M, M_1)$.
    Then $(I \otimes_{W(R)} L, I \otimes_{W(R)} T)$ is a normal decomposition of $I \otimes (M, M_1)$ and we can define the desired isomorphism as
    \[
        (I \otimes_{W(R)} M_1)^{\sim} \cong (I \otimes_{W(R)} L)^{\sigma} \oplus (I \otimes_{W(R)} T)^{\sigma} \to I^{\sigma} \otimes_{W(R)} (L^{\sigma} \oplus T^{\sigma}) \cong I^{\sigma} \otimes_{W(R)} \widetilde{M_1}. \qedhere
    \]
\end{proof}

\begin{definition}
    Let $(M, M_1, \Psi)$ be a display over $R$ and let $(I, \iota)$ be a tuple consisting of a finite projective $W(R)$-module $I$ and an isomorphism $\iota \colon I^{\sigma} \to I$.
    Then we define the \emph{twist}
    \[
        (I, \iota) \otimes (M, M_1, \Psi) \coloneqq (I \otimes_{W(R)} M, I \otimes_{W(R)} M_1, \iota \otimes \Psi),
    \]
    where we implicitly use \Cref{lem:m-1-tilde-twist} to make sense of $\iota \otimes \Psi$ as an isomorphism $(I \otimes_{W(R)} M_1)^{\sim} \to M$.
    This defines an action of the symmetric monoidal category of tuples $(I, \iota)$ as above on the category of displays over $R$ and in fact this action is naturally compatible with dualities and the $\Zp(R)$-linear structure.

    We similarly define twists of $(m, n)$-truncated displays over $R$ by tuples $(I, \iota)$ consisting of a finite projective $W_m(R)$-module $I$ and an isomorphism $\iota \colon W_n(R) \otimes_{\sigma, W_m(R)} I \to W_n(R) \otimes_{W_m(R)} I$.
\end{definition}

\begin{remark}
    We can conceptually understand duals and twists of pairs and displays in terms of higher displays.

    To a pair $(M, M_1)$ there is a corresponding finite projective graded module over $W(R)^{\oplus}$ with type concentrated in $[0, 1]$ (see \Cref{rmk:m-1-tilde}).
    The dual pair $(M, M_1)^{\vee}$ then corresponds to taking the dual of that module and shifting it by $1$ and the twist $I \otimes (M, M_1)$ corresponds to viewing $I$ as a finite projective graded $W(R)^{\oplus}$-module with type concentrated in $0$ and forming the tensor product over $W(R)^{\oplus}$.
    The various categorical properties of taking duals and twists then all follow from the fact that finite projective graded modules over a graded ring form a rigid symmetric monoidal category.
\end{remark}

\subsection{The display of a $p$-divisible group} \label{subsec:disp-p-div}

We have the following result of Lau, relating $p$-divisible groups and displays.

\begin{theorem}[\cite{lau-10}] \label{thm:disp-p-div}
    There is a natural $\Zp(R)$-linear exact functor
    \[
        \DD \colon \curlybr[\big]{\text{$p$-divisible groups over $R$}}^{\op} \to \curlybr[\big]{\text{displays over $R$}}
    \]
    that is compatible with dualities and satisfies the following properties.
    \begin{itemize}
        \item
        For a $p$-divisible group $X$ over $R$ of height $h$ and dimension $d$ the display $\DD(X)$ is of type $(h, d)$.

        \item
        Suppose that $R$ is a perfect ring of characteristic $p$.
        Then $\DD$ coincides with classical (contravariant) Dieudonné theory.

        \item
        $\DD$ restricts to an equivalence
        \[
            \DD \colon \curlybr[\big]{\text{formal $p$-divisible groups over $R$}}^{\op} \to \curlybr[\big]{\text{$F$-nilpotent displays over $R$}}.
        \]

        \item
        Let $X$, $X'$ be $p$-divisible groups over $R$ of height $h$ and let $f \colon X' \to X$ be an isogeny of height $r \in \ZZ_{\geq 0}$.
        Assume that there exists an isogeny $g \colon X \to X'$ such that $g \circ f = p \cdot \id_{X'}$ and $f \circ g = p \cdot \id_X$ (note that this forces $r \leq h$).
        Then the homomorphism of $W(R)/pW(R)$-modules
        \[
            \DD(f) \colon \DD(X)/p\DD(X) \to \DD(X')/p\DD(X')
        \]
        is of constant rank $h - r$.
    \end{itemize}
\end{theorem}

\begin{proof}
    See \cite[Proposition 4.1 and Theorem 5.1]{lau-10}.
    For the last claim we can use \Cref{lem:chains-rank} to reduce to the situation that $R = k$ is an algebraically closed field of characteristic $p$ where the statement is a standard result from classical Dieudonné theory.
\end{proof}